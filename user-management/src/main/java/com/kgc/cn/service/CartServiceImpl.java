package com.kgc.cn.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.kgc.cn.common.model.dto.ShoppingCart;
import com.kgc.cn.common.model.dto.example.ShoppingCartExample;
import com.kgc.cn.common.model.vo.ShoppingCartVo;
import com.kgc.cn.common.service.user.CartService;
import com.kgc.cn.mapper.ShoppingCartMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Objects;

/**
 * Created by scy on 2019/12/23
 */

@Service
public class CartServiceImpl implements CartService {

    @Autowired
    private ShoppingCartMapper shoppingCartMapper;

    @Override
    public int addProductCartLogin(ShoppingCart shoppingCart) {
        ShoppingCartExample cartExample = new ShoppingCartExample();
        cartExample.createCriteria().andUserIdEqualTo(shoppingCart.getUserId()).andProductIdEqualTo(shoppingCart.getProductId());
        List<ShoppingCart> shoppingCartList = shoppingCartMapper.selectByExample(cartExample);
        if (!CollectionUtils.isEmpty(shoppingCartList)) {
            ShoppingCart cart = shoppingCartList.get(0);
            cart.setProductNum(shoppingCart.getProductNum() + cart.getProductNum());
            return shoppingCartMapper.updateByExample(cart, cartExample);
        }
        return shoppingCartMapper.insert(shoppingCart);
    }

    @Override
    public List<ShoppingCart> showProductCartLogin(String userId) {
        ShoppingCartExample cartExample = new ShoppingCartExample();
        cartExample.createCriteria().andUserIdEqualTo(userId);
        List<ShoppingCart> shoppingCartList = shoppingCartMapper.selectByExample(cartExample);
        return shoppingCartList;
    }

    @Override
    public PageInfo<ShoppingCart> showProductCartLogin(String userId, int page, int size) {
        PageInfo<ShoppingCart> infoVoPageInfo = PageHelper.startPage(page, size).doSelectPageInfo(() -> showProductCartLogin(userId));
        return infoVoPageInfo;
    }

    @Override
    public int delProductCartLogin(String productId) {
        ShoppingCartExample shoppingCartExample = new ShoppingCartExample();
        shoppingCartExample.createCriteria().andProductIdEqualTo(productId);
        return shoppingCartMapper.deleteByExample(shoppingCartExample);
    }

    @Override
    public ShoppingCartVo queryProductCart(String productId) {
        ShoppingCart shoppingCart = shoppingCartMapper.queryProductCart(productId);
        ShoppingCartVo shoppingCartVo = new ShoppingCartVo();
        if (Objects.isNull(shoppingCart)) {
            return null;
        }
        BeanUtils.copyProperties(shoppingCart, shoppingCartVo);
        return shoppingCartVo;
    }

    @Override
    public int delAllProductCartLogin(String userId) {
        ShoppingCartExample shoppingCartExample = new ShoppingCartExample();
        shoppingCartExample.createCriteria().andUserIdEqualTo(userId);
        return shoppingCartMapper.deleteByExample(shoppingCartExample);
    }

    @Override
    public int updateProductCartLogin(ShoppingCart shoppingCart) {
        ShoppingCartExample example = new ShoppingCartExample();
        example.createCriteria().andProductIdEqualTo(shoppingCart.getProductId())
                .andUserIdEqualTo(shoppingCart.getUserId());
        List<ShoppingCart> shoppingCartList = shoppingCartMapper.selectByExample(example);
        if (!CollectionUtils.isEmpty(shoppingCartList)) {
            ShoppingCart oldShoppingCart = shoppingCartList.get(0);
            int num = oldShoppingCart.getProductNum() - shoppingCart.getProductNum();
            if (num > 0) {
                oldShoppingCart.setProductNum(num);
            } else {
                shoppingCartMapper.deleteByExample(example);
            }
            return shoppingCartMapper.updateByExampleSelective(oldShoppingCart, example);
        }
        return 0;
    }

    @Override
    public int addCartLogin(ShoppingCart shoppingCart) {
        ShoppingCartExample example = new ShoppingCartExample();
        example.createCriteria().andProductIdEqualTo(shoppingCart.getProductId())
                .andUserIdEqualTo(shoppingCart.getUserId());
        List<ShoppingCart> shoppingCartList = shoppingCartMapper.selectByExample(example);
        if (!CollectionUtils.isEmpty(shoppingCartList)) {
            ShoppingCart oldShoppingCart = shoppingCartList.get(0);
            oldShoppingCart.setProductNum(oldShoppingCart.getProductNum() + shoppingCart.getProductNum());
            return shoppingCartMapper.updateByExampleSelective(oldShoppingCart, example);
        }
        return shoppingCartMapper.insert(shoppingCart);
    }

}
