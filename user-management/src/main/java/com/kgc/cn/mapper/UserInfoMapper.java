package com.kgc.cn.mapper;

import com.kgc.cn.common.model.dto.UserInfo;
import com.kgc.cn.common.model.dto.example.UserInfoExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserInfoMapper {
    long countByExample(UserInfoExample example);

    int deleteByExample(UserInfoExample example);

    int deleteByPrimaryKey(String userId);

    int insert(UserInfo record);

    int insertSelective(UserInfo record);

    List<UserInfo> selectByExample(UserInfoExample example);

    UserInfo selectByPrimaryKey(String userId);

    int updateByExampleSelective(@Param("record") UserInfo record, @Param("example") UserInfoExample example);

    int updateByExample(@Param("record") UserInfo record, @Param("example") UserInfoExample example);

    int updateByPrimaryKeySelective(UserInfo record);

    int updateByPrimaryKey(UserInfo record);

    List<UserInfo> select(@Param("record") UserInfo record);

    UserInfo phoneLogin(@Param("userPhone") String userPhone, @Param("userPassword") String userPassword);

    UserInfo mailLogin(@Param("userEmail") String userEmail, @Param("userPassword") String userPassword);

    int phoneBound(@Param("userPhone") String userPhone);

    int emailBound(@Param("userEmail") String userEmail);
}