package com.kgc.cn.mapper;

import com.kgc.cn.common.model.dto.OrderState;
import com.kgc.cn.common.model.dto.example.OrderStateExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface OrderStateMapper {
    long countByExample(OrderStateExample example);

    int deleteByExample(OrderStateExample example);

    int deleteByPrimaryKey(String stateId);

    int insert(OrderState record);

    int insertSelective(OrderState record);

    List<OrderState> selectByExample(OrderStateExample example);

    OrderState selectByPrimaryKey(String stateId);

    int updateByExampleSelective(@Param("record") OrderState record, @Param("example") OrderStateExample example);

    int updateByExample(@Param("record") OrderState record, @Param("example") OrderStateExample example);

    int updateByPrimaryKeySelective(OrderState record);

    int updateByPrimaryKey(OrderState record);
}