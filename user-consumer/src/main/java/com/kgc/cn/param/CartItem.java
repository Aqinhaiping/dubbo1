package com.kgc.cn.param;

import com.kgc.cn.common.model.vo.ProductInfoVo;
import com.kgc.cn.common.utils.stringid.StringIntUtils;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel("购物车元素")
@Data
public class CartItem {
    @ApiModelProperty("商品对象")
    private ProductInfoVo good;

    @ApiModelProperty(value = "商品数量", example = "1")
    private Integer amount = 1;


    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((good == null) ? 0 : StringIntUtils.stringToInt(good.getProductId()));
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        CartItem other = (CartItem) obj;
        if (good == null) {
            if (other.good != null)
                return false;
        } else if (!good.getProductId().equals(other.good.getProductId()))
            return false;

        return true;
    }

}