package com.kgc.cn.param;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@ApiModel("购物车")
@Data
public class Cart implements Serializable {

    private static final long serialVersionUID = -5187165198937756422L;

    @ApiModelProperty("商品列表")
    private List<CartItem> items = new ArrayList<>();

    //添加购物项到购物车
    public void addItem(CartItem item) {
        //判断是否包含同款
        if (items.contains(item)) {
            //追加数量
            for (CartItem cartItem : items) {
                if (cartItem.getGood().getProductId().equals(item.getGood().getProductId())) {
                    cartItem.setAmount(item.getAmount() + cartItem.getAmount());
                }
            }
        } else {
            items.add(item);
        }
    }

    //消减购物在购物车
    public void removeItem(CartItem item) {
        // 判断是否包含同款
        if (items.contains(item)) {
            //消减数量
            boolean flag = false;
            for (CartItem cartItem : items) {
                if (cartItem.getGood().getProductId().equals(item.getGood().getProductId())) {
                    int num = (cartItem.getAmount() - item.getAmount());
                    if (num <= 0) {
                        flag = true;
                    } else {
                        cartItem.setAmount(cartItem.getAmount() - item.getAmount());
                    }
                }
            }
            if (flag) items.remove(item);
        }
    }

}
