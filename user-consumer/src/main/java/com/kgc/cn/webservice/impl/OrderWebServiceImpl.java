package com.kgc.cn.webservice.impl;

import com.alibaba.dubbo.config.annotation.Reference;
import com.kgc.cn.common.enums.NameSpaceEnum;
import com.kgc.cn.common.model.dto.OrderDetail;
import com.kgc.cn.common.model.dto.ShoppingCart;
import com.kgc.cn.common.model.vo.OrderDetailVo;
import com.kgc.cn.common.model.vo.UserInfoVo;
import com.kgc.cn.common.service.third.WxService;
import com.kgc.cn.common.service.user.CartService;
import com.kgc.cn.common.service.user.OrderService;
import com.kgc.cn.common.utils.copy.CopyUtils;
import com.kgc.cn.utils.redis.RedisUtils;
import com.kgc.cn.webservice.OrderWebService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Created by scy on 2019/12/23
 */

@Service
public class OrderWebServiceImpl implements OrderWebService {

    @Reference
    WxService wxService;
    @Reference
    OrderService orderService;
    @Reference
    private CartService cartService;
    @Autowired
    RedisUtils redisUtils;


    @Override
    public String payOrder(String orderId, UserInfoVo current) throws Exception {
        OrderDetailVo orderDetailVo = orderService.selectOrder(orderId, current.getUserId());
        if (Objects.isNull(orderDetailVo)) return null;
        OrderDetail orderDetail = CopyUtils.copy(orderDetailVo, OrderDetail.class);
        Map<String, String> map = wxService.pay(orderDetail);
        return "SUCCESS".equals(map.get("result_code")) ? map.get("code_url") : null;
    }

    @Override
    public String payVip(UserInfoVo userInfoVo) throws Exception {
        OrderDetail orderDetail = new OrderDetail().createId();
        orderDetail.setOrderState(20001);
        orderDetail.setUserId(userInfoVo.getUserId());
        orderDetail.setProductNum(1);
        orderDetail.setProductId("vip");
        orderService.addOrder(orderDetail);
        redisUtils.set(NameSpaceEnum.VIP.getNamespace() + orderDetail.getOrderId(), userInfoVo.getUserId(), 600);
        Map<String, String> map = wxService.pay(orderDetail);
        return "SUCCESS".equals(map.get("result_code")) ? map.get("code_url") : null;
    }

    @Override
    public boolean shoppingCartSettlement(String userId) {
        List<ShoppingCart> shoppingCartList = cartService.showProductCartLogin(userId);
        if (CollectionUtils.isEmpty(shoppingCartList)) {
            return false;
        }
        shoppingCartList.forEach(shoppingCarts -> {
            OrderDetail orderDetail = new OrderDetail().createId();
            orderDetail.setUserId(userId);
            orderDetail.setProductId(shoppingCarts.getProductId());
            orderDetail.setProductNum(shoppingCarts.getProductNum());
            orderDetail.setOrderState(20001);
            orderService.addOrder(orderDetail);
            String key = NameSpaceEnum.PRODUCT_AMOUNT.getNamespace() + shoppingCarts.getProductId();
            redisUtils.decr(key, shoppingCarts.getProductNum());
            redisUtils.set(orderDetail.getOrderId(), "", 1200);
        });
        cartService.delAllProductCartLogin(userId);
        return true;
    }

}
