package com.kgc.cn.webservice;

import com.github.pagehelper.PageInfo;
import com.kgc.cn.common.model.dto.ProductInfo;
import com.kgc.cn.common.model.dto.ShoppingCart;
import com.kgc.cn.common.model.param.ProductDetailsParam;
import com.kgc.cn.common.model.vo.UserInfoVo;
import com.kgc.cn.param.Cart;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * Created by scy on 2019/12/23
 */
public interface CartWebService {

    /**
     * 商品详情展示
     *
     * @param productId 商品id
     * @return 商品信息
     */
    ProductDetailsParam queryProductInfo(String productId);

    /**
     * 添加购物车（未登录）
     *
     * @param productId  商品id
     * @param productNum 商品数量
     * @param request    请求
     * @return true或者false
     */
    int addProductCartNotLogin(String productId, int productNum, HttpServletRequest request);

    /**
     * 购物车商品详情展示（未登录）
     *
     * @param request 请求
     * @return 商品信息
     */
    Cart showProductCartNotLogin(HttpServletRequest request);

    /**
     * 购物车商品详情展示（登录时）
     *
     * @param userId 用户id
     * @return list集合
     */
    PageInfo<ShoppingCart> showProductCartLogin(String userId, int page, int size);

    /**
     * 购物车商品增加（登录时同步）
     *
     * @param request 请求
     * @param current 当前用户信息
     * @return 影响条数
     */
    int addProductCartLogin(HttpServletRequest request, UserInfoVo current);


    /**
     * 清空购物车（未登录）
     *
     * @param request 请求
     * @return true或false
     */
    boolean delAllProductCartNotLogin(HttpServletRequest request);

    /**
     * 购物车商品删除（登录时）
     *
     * @param productId 商品id
     * @param current   当前用户
     * @return true或false
     */
    boolean delProductCartLogin(String productId, UserInfoVo current);

    /**
     * 清空购物车（登录时）
     *
     * @param current 当前用户
     * @return true或false
     */
    boolean delAllProductCartLogin(UserInfoVo current);

    /**
     * 商品数量减少（未登录）
     *
     * @param productId  商品id
     * @param productNum 商品库存
     * @param request    请求
     * @return true或false
     */
    int removeProductCartNotLogin(String productId, int productNum, HttpServletRequest request);

    /**
     * 购物车商品增加（登录时）
     *
     * @param productId  商品id
     * @param productNum 商品数量
     * @param current    当前用户
     * @return true或false
     */
    int addCartLogin(String productId, int productNum, UserInfoVo current);

    /**
     * @param productId
     * @param current
     * @return
     */
    int updateProductCartLogin(String productId, int productNum, UserInfoVo current);

    /**
     * 商品查询
     *
     * @return
     */

    List<Map<String, Map<String, List<ProductInfo>>>> merchandise();
}
