package com.kgc.cn.webservice;

import com.kgc.cn.common.model.vo.UserInfoVo;

/**
 * Created by scy on 2019/12/23
 */
public interface OrderWebService {

    /**
     * 订单支付
     *
     * @param orderId 订单id
     */
    String payOrder(String orderId, UserInfoVo current) throws Exception;

    String payVip(UserInfoVo userInfoVo) throws Exception;

    /**
     * 结算购物车
     *
     * @param userId 用户id
     * @return true或false
     */
    boolean shoppingCartSettlement(String userId);


}
