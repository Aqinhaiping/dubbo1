package com.kgc.cn.webservice;

import com.kgc.cn.common.model.param.UserInfoParam;
import com.kgc.cn.common.model.vo.UserInfoVo;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by scy on 2019/12/23
 */


public interface UserWebService {
    /**
     * 用户注册
     *
     * @param userInfoParam
     * @return
     */
    int registered(UserInfoParam userInfoParam);

    /**
     * 登录
     *
     * @param
     * @return
     */
    String Login(String account, String userPassword, HttpServletRequest request);

    /**
     * 微信登录
     *
     * @param code
     * @return
     */
    String wxLogin(String code, HttpServletRequest request, String state);

    /**
     * 用户查询
     *
     * @param userInfoVo
     * @return
     */
    List<UserInfoVo> select(UserInfoVo userInfoVo);

    /**
     * 开通会员
     */
    boolean openVip(String orderId, int day, UserInfoVo userInfoVo);

    void relogin(String userId, HttpServletRequest request);

}
