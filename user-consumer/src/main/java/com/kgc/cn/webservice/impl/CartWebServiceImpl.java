package com.kgc.cn.webservice.impl;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.excel.util.CollectionUtils;
import com.alibaba.excel.util.StringUtils;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.kgc.cn.common.model.dto.ProductInfo;
import com.kgc.cn.common.model.dto.ShoppingCart;
import com.kgc.cn.common.model.param.ProductDetailsParam;
import com.kgc.cn.common.model.param.ProductInfoParam;
import com.kgc.cn.common.model.vo.ProductInfoVo;
import com.kgc.cn.common.model.vo.ShoppingCartVo;
import com.kgc.cn.common.model.vo.UserInfoVo;
import com.kgc.cn.common.service.management.ProductService;
import com.kgc.cn.common.service.user.CartService;
import com.kgc.cn.common.utils.ip.IPUtils;
import com.kgc.cn.param.Cart;
import com.kgc.cn.param.CartItem;
import com.kgc.cn.utils.redis.RedisUtils;
import com.kgc.cn.webservice.CartWebService;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * Created by scy on 2019/12/23
 */

@Service
@Log4j
public class CartWebServiceImpl implements CartWebService {

    @Reference
    private ProductService productService;

    @Autowired
    private RedisUtils redisUtils;

    @Reference
    private CartService cartService;


    private String cartNameSpace = "cartNameSpace:";

    @Override
    public ProductDetailsParam queryProductInfo(String productId) {
        return productService.queryProductInfo(productId);
    }


    @Override
    public int addProductCartNotLogin(String productId, int productNum, HttpServletRequest request) {
        String ip = IPUtils.getIp(request);
        ProductInfoParam productInfoParam = new ProductInfoParam();
        productInfoParam.setProductId(productId);
        List<ProductInfoVo> productInfoVoList = productService.show(productInfoParam);
        if (CollectionUtils.isEmpty(productInfoVoList)) {
            return 3;
        }
        CartItem cartItem = new CartItem();
        cartItem.setGood(productInfoVoList.get(0));
        if (productInfoVoList.get(0).getProductStock() < productNum) {
            return 2;
        }
        cartItem.setAmount(productNum);
        if (redisUtils.hasKey(cartNameSpace + ip)) {
            String cartJson = redisUtils.get(cartNameSpace + ip).toString();
            Cart cart = JSONObject.parseObject(cartJson, Cart.class);
            cart.addItem(cartItem);
            redisUtils.set(cartNameSpace + ip, JSONObject.toJSONString(cart), 30 * 24 * 3600);
        } else {
            Cart cart = new Cart();
            cart.addItem(cartItem);
            String cartJson = JSONObject.toJSONString(cart);
            redisUtils.set(cartNameSpace + ip, cartJson, 30 * 24 * 3600);
        }
        return 1;
    }

    @Override
    public Cart showProductCartNotLogin(HttpServletRequest request) {
        String ip = IPUtils.getIp(request);
        if (redisUtils.hasKey(cartNameSpace + ip)) {
            String cartJson = redisUtils.get(cartNameSpace + ip).toString();
            Cart cart = JSONObject.parseObject(cartJson, Cart.class);
            return cart;
        }
        return null;
    }


    @Override
    public PageInfo<ShoppingCart> showProductCartLogin(String userId, int page, int size) {
        PageInfo<ShoppingCart> pageInfo = cartService.showProductCartLogin(userId, page, size);
        return pageInfo;
    }

    @Override
    public int addProductCartLogin(HttpServletRequest request, UserInfoVo current) {
        String ip = IPUtils.getIp(request);
        if (redisUtils.hasKey(cartNameSpace + ip)) {
            String cartJson = redisUtils.get(cartNameSpace + ip).toString();
            Cart cart = JSONObject.parseObject(cartJson, Cart.class);
            List<CartItem> cartItemList = cart.getItems();
            if (!CollectionUtils.isEmpty(cartItemList)) {
                cartItemList.forEach(cartItem -> {
                    ShoppingCart shoppingCart = new ShoppingCart();
                    shoppingCart.setUserId(current.getUserId());
                    shoppingCart.setProductId(cartItem.getGood().getProductId());
                    shoppingCart.setProductNum(cartItem.getAmount());
                    cartService.addProductCartLogin(shoppingCart);
                    redisUtils.del(cartNameSpace + ip);
                });
                return 2;
            }
        }
        return 1;
    }


    @Override
    public boolean delAllProductCartNotLogin(HttpServletRequest request) {
        String ip = IPUtils.getIp(request);
        if (redisUtils.hasKey(cartNameSpace + ip)) {
            redisUtils.del(cartNameSpace + ip);
            return true;
        }
        return false;
    }

    @Override
    public boolean delAllProductCartLogin(UserInfoVo current) {
        String id = current.getUserId();
        cartService.delAllProductCartLogin(id);
        return true;
    }

    @Override
    public int removeProductCartNotLogin(String productId, int productNum, HttpServletRequest request) {
        String ip = IPUtils.getIp(request);

        if (redisUtils.hasKey(cartNameSpace + ip)) {
            String cartJson = redisUtils.get(cartNameSpace + ip).toString();
            Cart cart = JSONObject.parseObject(cartJson, Cart.class);
            ProductInfoParam productInfoParam = new ProductInfoParam();
            productInfoParam.setProductId(productId);
            List<ProductInfoVo> productInfoVoList = productService.show(productInfoParam);
            if (CollectionUtils.isEmpty(productInfoVoList)) {
                return 1;
            }
            CartItem cartItem = new CartItem();
            cartItem.setGood(productInfoVoList.get(0));
            cartItem.setAmount(productNum);
            cart.removeItem(cartItem);
            redisUtils.set(cartNameSpace + ip, JSONObject.toJSONString(cart));
            if (CollectionUtils.isEmpty(cart.getItems())) {
                redisUtils.del(cartNameSpace + ip);
            }
            return 2;
        }
        return 3;
    }

    @Override
    public boolean delProductCartLogin(String productId, UserInfoVo current) {
        ShoppingCartVo shoppingCartVo = cartService.queryProductCart(productId);
        if (StringUtils.isEmpty(shoppingCartVo)) {
            return false;
        }
        cartService.delProductCartLogin(productId);
        return true;
    }

    @Override
    public int addCartLogin(String productId, int productNum, UserInfoVo current) {
        ShoppingCart shoppingCart = new ShoppingCart();
        ProductInfoParam productInfoParam = new ProductInfoParam();
        shoppingCart.setUserId(current.getUserId());
        shoppingCart.setProductId(productId);
        List<ProductInfoVo> productInfoVoList = productService.show(productInfoParam);
        if (productInfoVoList.get(0).getProductStock() < productNum) {
            return 2;
        }
        if (StringUtils.isEmpty(shoppingCart)) {
            return 3;
        }
        shoppingCart.setProductNum(productNum);
        cartService.addCartLogin(shoppingCart);
        return 1;
    }

    @Override
    public int updateProductCartLogin(String productId, int productNum, UserInfoVo current) {
        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.setUserId(current.getUserId());
        shoppingCart.setProductId(productId);
        if (StringUtils.isEmpty(shoppingCart)) {
            return 2;
        }
        if (null == (shoppingCart.getProductNum())) {
            return 3;
        }
        shoppingCart.setProductNum(productNum);
        cartService.updateProductCartLogin(shoppingCart);
        return 1;
    }

    @Override
    public List<Map<String, Map<String, List<ProductInfo>>>> merchandise() {
        return productService.merchandise();
    }
}
