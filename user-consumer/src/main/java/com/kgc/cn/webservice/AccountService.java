package com.kgc.cn.webservice;

import com.kgc.cn.common.model.vo.UserInfoVo;

/**
 * Created by boot on 2019/12/25
 */
public interface AccountService {

    void SendSms(String phone, UserInfoVo userInfoVo);

    void SendEmail(String email, UserInfoVo userInfoVo);

    int boundPhone(String phone, String code, UserInfoVo userInfoVo);

    int boundEmail(String email, String code, UserInfoVo userInfoVo);

    String boundWx(String openId, String token);
}
