package com.kgc.cn.webservice.impl;

import com.alibaba.dubbo.common.utils.StringUtils;
import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Maps;
import com.kgc.cn.common.enums.MailTemplates;
import com.kgc.cn.common.enums.NameSpaceEnum;
import com.kgc.cn.common.model.param.SmsParam;
import com.kgc.cn.common.model.param.mail.MailParam;
import com.kgc.cn.common.model.vo.UserInfoVo;
import com.kgc.cn.common.service.third.AliService;
import com.kgc.cn.common.service.third.MailService;
import com.kgc.cn.common.service.user.UserService;
import com.kgc.cn.common.utils.randoms.RandomsUtils;
import com.kgc.cn.utils.redis.RedisUtils;
import com.kgc.cn.webservice.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * Created by boot on 2019/12/25
 */
@Service
public class AccountServiceImpl implements AccountService {
    @Reference
    AliService aliService;

    @Reference
    MailService mailService;

    @Reference
    UserService userService;

    @Autowired
    RedisUtils redisUtils;

    @Override
    public void SendSms(String phone, UserInfoVo userInfoVo) {
        String code = RandomsUtils.getRandom(4);
        redisUtils.set(NameSpaceEnum.PHONE_CODE.getNamespace() + phone, code, 600);
        SmsParam smsParam = new SmsParam();
        smsParam.setToPhone(phone);
        smsParam.setTemplateCode("SMS_180955764");
        Map<String, String> params = Maps.newHashMap();
        params.put("code", code);
        smsParam.setTemplateParams(params);
        aliService.sendSms(smsParam);
    }

    @Override
    public void SendEmail(String email, UserInfoVo userInfoVo) {
        String code = RandomsUtils.getRandom(4);
        MailParam mailParam = new MailParam();
        redisUtils.set(NameSpaceEnum.EMAIL_CODE.getNamespace() + email, code, 600);
        mailParam.setTo(email);
        mailParam.setSubject("验证码");
        Map<String, Object> params = Maps.newHashMap();
        params.put("username", userInfoVo.getNickname());
        params.put("code", code);
        mailParam.setTemplateParams(params);
        mailService.sendTemplateMail(mailParam, MailTemplates.VERIFY);
    }

    @Override
    public int boundPhone(String phone, String code, UserInfoVo userInfoVo) {
        if (!code.equals(redisUtils.get(NameSpaceEnum.PHONE_CODE.getNamespace() + phone))) {
            return 2;
        }
        if (!StringUtils.isEmpty(userInfoVo.getUserPhone())) {
            return 3;
        }
        userInfoVo.setUserPhone(phone);
        return userService.updateUser(userInfoVo) == 1 ? 1 : 4;
    }

    @Override
    public int boundEmail(String email, String code, UserInfoVo userInfoVo) {
        if (!code.equals(redisUtils.get(NameSpaceEnum.EMAIL_CODE.getNamespace() + email))) {
            return 2;
        }
        if (!StringUtils.isEmpty(userInfoVo.getUserEmail())) {
            return 3;
        }
        userInfoVo.setUserEmail(email);
        return userService.updateUser(userInfoVo) == 1 ? 1 : 4;
    }

    @Override
    public String boundWx(String openId, String token) {
        String userJson = (String) redisUtils.get(token);
        if (!StringUtils.isEmpty(userJson)) {
            UserInfoVo infoVo = JSONObject.parseObject(userJson, UserInfoVo.class);
            if (StringUtils.isEmpty(infoVo.getUserOpenid())) {
                infoVo.setUserOpenid(openId);
                userService.updateUser(infoVo);
                return "true";
            }
        }
        return "false";
    }
}
