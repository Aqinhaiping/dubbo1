package com.kgc.cn.webservice.impl;

import com.alibaba.dubbo.common.utils.StringUtils;
import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.excel.util.CollectionUtils;
import com.alibaba.fastjson.JSONObject;
import com.kgc.cn.common.enums.NameSpaceEnum;
import com.kgc.cn.common.model.dto.VipRecord;
import com.kgc.cn.common.model.param.UserInfoParam;
import com.kgc.cn.common.model.vo.UserInfoVo;
import com.kgc.cn.common.service.third.WxService;
import com.kgc.cn.common.service.user.UserService;
import com.kgc.cn.common.utils.client.UrlUtils;
import com.kgc.cn.common.utils.copy.CopyUtils;
import com.kgc.cn.common.utils.time.TimeUtils;
import com.kgc.cn.utils.redis.RedisUtils;
import com.kgc.cn.webservice.UserWebService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import static java.util.UUID.randomUUID;

/**
 * Created by scy on 2019/12/23
 */

@Service
public class UserWebServiceImpl implements UserWebService {
    @Reference
    private UserService userService;
    @Autowired
    private RedisUtils redisUtils;

    @Reference(timeout = 30000)
    private WxService wxService;

    @Override
    public int registered(UserInfoParam userInfoParam) {
        String phone = userInfoParam.getUserPhone();
        String mail = userInfoParam.getUserEmail();
        if (StringUtils.isEmpty(phone) && StringUtils.isEmpty(mail)) {
            return 0;
        }
        if (StringUtils.isNotEmpty(phone)) {
            if (!CollectionUtils.isEmpty(userService.select(UserInfoVo.builder().userPhone(phone).build()))) {
                return 2;
            }
        }
        if (StringUtils.isNotEmpty(mail)) {
            if (!CollectionUtils.isEmpty(userService.select(UserInfoVo.builder().userEmail(mail).build()))) {
                return 3;
            }
        }
        return userService.registered(CopyUtils.copy(userInfoParam, UserInfoVo.class).createId());
    }

    @Override
    public String Login(String account, String userPassword, HttpServletRequest request) {
        if (!account.contains("@")) {
            UserInfoVo userphoneVo = userService.phoneLogin(account, userPassword);
            if (Objects.isNull(userphoneVo)) {
                return "-1";
            }
            String json = JSONObject.toJSONString(userphoneVo);
            String token = request.getSession().getId();
            redisUtils.set(token, json, 2000);
            return token;
        } else {
            UserInfoVo userEmailVo = userService.mailLogin(account, userPassword);
            if (Objects.isNull(userEmailVo)) {
                return "-1";
            }
            String json = JSONObject.toJSONString(userEmailVo);
            String token = request.getSession().getId();
            redisUtils.set(token, json, 2000);
            return token;
        }
    }


    @Override
    public boolean openVip(String orderId, int day, UserInfoVo userInfoVo) {
        long secounds = 0;
        String key = NameSpaceEnum.VIP.getNamespace() + userInfoVo.getUserId();
        if (redisUtils.hasKey(key)) secounds = redisUtils.getExpire(key);
        redisUtils.set(key, 1, secounds + (day - 1) * 24 * 60 * 60 + 10);
        VipRecord record = VipRecord.builder().userId(userInfoVo.getUserId())
                .startTime(TimeUtils.getNextDay(new Date(), 0))
                .endTime(TimeUtils.getNextDay(new Date(), day)).build();
        userService.addVipRecord(record);
        userInfoVo.setCategoryId("10002");
        userService.updateUser(userInfoVo);
        return true;
    }

    @Override
    public void relogin(String userId, HttpServletRequest request) {
        String token = request.getHeader("token");
        if (!StringUtils.isEmpty(token)) {
            UserInfoVo userInfoVos = userService.select(UserInfoVo.builder().userId(userId).build()).get(0);
            if (!Objects.isNull(userInfoVos)) {
                String json = JSONObject.toJSONString(userInfoVos);
                redisUtils.set(token, json, 2000);
            }
        }
    }


    @Override
    public String wxLogin(String code, HttpServletRequest request, String state) {
        String userJson = wxService.getUserInfo(code);
        JSONObject jsonObject = JSONObject.parseObject(userJson);
        String openId = jsonObject.getString("openid");

        String flag = UrlUtils.loadURL("http://b9aaac51.ngrok.io/account/boundWx" + "?openId=" + openId + "&token=" + state);
        assert flag != null;
        if ("true".equals(flag.replaceAll("[\\t\\n\\r]", ""))) {
            return "10004";
        }

        List<UserInfoVo> userInfos = userService.select(UserInfoVo.builder().userOpenid
                (openId).build());
        if (CollectionUtils.isEmpty(userInfos)) {
            UserInfoVo userInfoVo = new UserInfoVo().createId();
            userInfoVo.setNickname(jsonObject.getString("nickname"));
            userInfoVo.setUserOpenid(jsonObject.getString("openid"));
            userInfoVo.setUserSex(Integer.valueOf(jsonObject.getString("sex")));
            userInfoVo.setUserPassword(randomUUID().toString().substring(0, 8));
            userInfoVo.setUserAge(20);
            userService.registered(userInfoVo);
            UserInfoVo infoVo = userService.select(UserInfoVo.builder().userOpenid(openId).build()).get(0);
            String json = JSONObject.toJSONString(infoVo);
            redisUtils.set(state, json, 2000);
            return "10003";
        }
        String json = JSONObject.toJSONString(userInfos.get(0));
        redisUtils.set(state, json, 2000);
        return "10003";
    }

    @Override
    public List<UserInfoVo> select(UserInfoVo userInfoVo) {
        List<UserInfoVo> infoList = userService.select(userInfoVo);
        return infoList;
    }

}




