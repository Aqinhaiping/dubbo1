package com.kgc.cn.controller;

import com.github.pagehelper.PageInfo;
import com.kgc.cn.common.enums.ReturnEnum;
import com.kgc.cn.common.model.dto.ProductInfo;
import com.kgc.cn.common.model.dto.ShoppingCart;
import com.kgc.cn.common.model.param.ProductDetailsParam;
import com.kgc.cn.common.model.vo.UserInfoVo;
import com.kgc.cn.common.utils.ReturnResult;
import com.kgc.cn.common.utils.result.ReturnResultUtils;
import com.kgc.cn.config.aop.CurrentUser;
import com.kgc.cn.config.aop.LoginRequired;
import com.kgc.cn.param.Cart;
import com.kgc.cn.webservice.CartWebService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Created by scy on 2019/12/23
 */

@Api(tags = "购物车管理")
@RestController
@RequestMapping(value = "/shopCar")
public class CartController {

    @Autowired
    private CartWebService cartWebService;

    /**
     * 商品详情展示
     *
     * @param productId 商品id
     * @return 统一返回
     */
    @ApiOperation(value = "商品详情展示")
    @PostMapping(value = "/showProductInfo")
    public ReturnResult showProductInfo(@ApiParam(value = "商品编号") @RequestParam String productId) {
        ProductDetailsParam productDetails = cartWebService.queryProductInfo(productId);
        if (Objects.isNull(productDetails)) {
            return ReturnResultUtils.returnFail(ReturnEnum.GoodEnum.NONE);
        }
        return ReturnResultUtils.returnSuccess(productDetails);
    }


    /**
     * 购物车商品增加（未登录）
     *
     * @param productId  商品id
     * @param productNum 商品数量
     * @param request    请求
     * @return 统一返回
     */
    @ApiOperation(value = "购物车商品增加（未登录）")
    @PostMapping(value = "/addCartNotLogin")
    public ReturnResult addCartNotLogin(@ApiParam(value = "商品id") @RequestParam String productId, @ApiParam(value = "商品数量") @RequestParam int productNum, HttpServletRequest request) {
        int count = cartWebService.addProductCartNotLogin(productId, productNum, request);
        if (count == 3) {
            return ReturnResultUtils.returnFail(ReturnEnum.GoodEnum.NONE);
        }
        if (count == 2) {
            return ReturnResultUtils.returnFail(ReturnEnum.GoodEnum.NUM_FAIL);
        }
        return ReturnResultUtils.returnSuccess();
    }

    /**
     * 购物车商品详情展示（未登录）
     *
     * @param request 请求
     * @return 统一返回
     */
    @ApiOperation(value = "购物车商品详情展示（未登录）")
    @PostMapping(value = "/showCartNotLogin")
    public ReturnResult showCartNotLogin(HttpServletRequest request) {
        Cart cart = cartWebService.showProductCartNotLogin(request);
        if (Objects.isNull(cart)) {
            return ReturnResultUtils.returnFail(ReturnEnum.CartEnum.NONE);
        }
        return ReturnResultUtils.returnSuccess(cart);
    }

    /**
     * 购物车商品详情展示（登录）
     *
     * @param current 当前用户
     * @param page    当前页数
     * @param size    每页条数
     * @return 统一返回
     */
    @LoginRequired
    @ApiOperation(value = "购物车商品详情展示（登录）")
    @PostMapping(value = "/showCartLogin")
    public ReturnResult showCartLogin(@CurrentUser @ApiIgnore UserInfoVo current,
                                      @ApiParam(value = "当前页数", example = "1", required = true) @RequestParam int page,
                                      @ApiParam(value = "每页条数", example = "5", required = true) @RequestParam int size) {
        PageInfo<ShoppingCart> pageInfo = cartWebService.showProductCartLogin(current.getUserId(), page, size);
        return ReturnResultUtils.returnSuccess(pageInfo);
    }

    /**
     * 购物车商品同步（登录时同步）
     *
     * @param request 请求
     * @param current 当前用户信息
     * @return 统一返回
     */
    @LoginRequired
    @ApiOperation(value = "购物车商品同步（登录时）")
    @PostMapping(value = "/addCartLogin")
    public ReturnResult addCartLogin(HttpServletRequest request, @CurrentUser @ApiIgnore UserInfoVo current) {
        int count = cartWebService.addProductCartLogin(request, current);
        if (count == 2) {
            return ReturnResultUtils.returnSuccess();
        }
        return ReturnResultUtils.returnFail(ReturnEnum.GoodEnum.ADD_FAIL);
    }


    /**
     * 清空购物车（未登录）
     *
     * @param request 请求
     * @return 统一返回
     */
    @ApiOperation(value = "清空购物车（未登录）")
    @PostMapping(value = "/delAllProductCartNotLogin")
    public ReturnResult delAllProductCartNotLogin(HttpServletRequest request) {
        if (cartWebService.delAllProductCartNotLogin(request)) {
            return ReturnResultUtils.returnSuccess();
        }
        return ReturnResultUtils.returnFail(ReturnEnum.CartEnum.DEL_FAIL);
    }

    /**
     * 购物车商品数量减少（未登录）
     *
     * @param productId  商品id
     * @param productNum 商品数量
     * @param request    请求
     * @return 统一返回
     */
    @ApiOperation(value = "购物车商品数量减少（未登录）")
    @PostMapping(value = "/removeProductCartNotLogin")
    public ReturnResult removeProductCartNotLogin(@ApiParam(value = "商品id") @RequestParam String productId, @ApiParam(value = "商品数量") @RequestParam int productNum, HttpServletRequest request) {
        int count = cartWebService.removeProductCartNotLogin(productId, productNum, request);
        if (count == 2) {
            return ReturnResultUtils.returnSuccess();
        }
        if (count == 1) {
            return ReturnResultUtils.returnFail(ReturnEnum.GoodEnum.NONE);
        }
        return ReturnResultUtils.returnFail(ReturnEnum.GoodEnum.NONE);
    }

    /**
     * 购物车商品删除（登录）
     *
     * @param productId 商品id
     * @param current   当前用户
     * @return 统一返回
     */
    @LoginRequired
    @ApiOperation(value = "购物车商品删除（登录）")
    @PostMapping(value = "/delProductCartLogin")
    public ReturnResult delProductCartLogin(@ApiParam(value = "商品id") @RequestParam String productId, @CurrentUser @ApiIgnore UserInfoVo current) {
        if (cartWebService.delProductCartLogin(productId, current)) {
            return ReturnResultUtils.returnSuccess();
        }
        return ReturnResultUtils.returnFail(ReturnEnum.ShoppingCarEnum.DEL_FAIL);
    }

    /**
     * 清空购物车（登录）
     *
     * @param current 当前用户
     * @return 统一返回
     */
    @LoginRequired
    @ApiOperation(value = "清空购物车（登录）")
    @PostMapping(value = "/delAllProductCartLogin")
    public ReturnResult delAllProductCartLogin(@CurrentUser @ApiIgnore UserInfoVo current) {
        if (cartWebService.delAllProductCartLogin(current)) {
            return ReturnResultUtils.returnSuccess();
        }
        return ReturnResultUtils.returnFail(ReturnEnum.ShoppingCarEnum.DEL_ALL_FAIL);
    }

    /**
     * 购物车商品数量增加（登录）
     *
     * @param productId  商品id
     * @param productNum 商品数量
     * @param current    当前用户
     * @return
     */
    @LoginRequired
    @ApiOperation(value = "购物车商品数量增加（登录）")
    @PostMapping(value = "/addProductCartLogin")
    public ReturnResult addProductCartLogin(@ApiParam(value = "商品id") @RequestParam String productId, @ApiParam(value = "商品数量") @RequestParam int productNum, @CurrentUser @ApiIgnore UserInfoVo current) {
        int count = cartWebService.addCartLogin(productId, productNum, current);
        if (count == 2) {
            return ReturnResultUtils.returnFail(ReturnEnum.GoodEnum.NUM_FAIL);
        }
        if (count == 2) {
            return ReturnResultUtils.returnFail(ReturnEnum.ShoppingCarEnum.FAIL);
        }
        return ReturnResultUtils.returnSuccess();
    }

    /**
     * 购物车商品数量减少（登录）
     *
     * @param productId  商品id
     * @param productNum 商品数量
     * @param current    当前用户
     * @return
     */
    @LoginRequired
    @ApiOperation(value = "购物车商品数量减少（登录）")
    @PostMapping(value = "/updateCart")
    public ReturnResult updateCart(@ApiParam(value = "商品id") @RequestParam String productId, @ApiParam(value = "商品数量") @RequestParam int productNum,
                                   @CurrentUser @ApiIgnore UserInfoVo current) {
        int count = cartWebService.updateProductCartLogin(productId, productNum, current);
        if (count == 2) {
            return ReturnResultUtils.returnFail(ReturnEnum.ShoppingCarEnum.DEL_ALL_FAIL);
        }
        if (count == 3) {
            return ReturnResultUtils.returnFail(ReturnEnum.GoodEnum.NONE);
        }
        return ReturnResultUtils.returnSuccess();
    }

    /**
     * 查询商品
     *
     * @return
     */
    @ApiOperation(value = "商品查询")
    @PostMapping(value = "/merchandise")
    public ReturnResult merchandise() {
        List<Map<String, Map<String, List<ProductInfo>>>> list = cartWebService.merchandise();
        return ReturnResultUtils.returnSuccess(list);
    }
}

