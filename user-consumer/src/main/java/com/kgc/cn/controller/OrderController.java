package com.kgc.cn.controller;

import com.alibaba.dubbo.common.utils.StringUtils;
import com.kgc.cn.common.enums.ReturnEnum;
import com.kgc.cn.common.model.vo.UserInfoVo;
import com.kgc.cn.common.utils.ReturnResult;
import com.kgc.cn.common.utils.result.ReturnResultUtils;
import com.kgc.cn.config.aop.CurrentUser;
import com.kgc.cn.config.aop.LoginRequired;
import com.kgc.cn.webservice.OrderWebService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;


/**
 * Created by scy on 2019/12/23
 */

@Api(tags = "订单管理")
@RestController
@RequestMapping("/order")
public class OrderController {

    @Autowired
    OrderWebService orderWebService;

    @LoginRequired
    @ApiOperation("支付会员")
    @PostMapping(value = "payVip")
    public ReturnResult payVip(@CurrentUser @ApiIgnore UserInfoVo userInfoVo) {
        String code = null;
        try {
            code = orderWebService.payVip(userInfoVo);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return StringUtils.isEmpty(code) ? ReturnResultUtils.returnFail(ReturnEnum.OrderEnum.ERROR) :
                ReturnResultUtils.returnSuccess(code);
    }

    /**
     * 结算购物车
     *
     * @param current 当前用户
     * @return
     */
    @LoginRequired
    @ApiOperation(value = "结算购物车")
    @PostMapping(value = "/shoppingCartSettlement")
    public ReturnResult shoppingCartSettlement(@CurrentUser @ApiIgnore UserInfoVo current) {
        if (orderWebService.shoppingCartSettlement(current.getUserId())) {
            return ReturnResultUtils.returnSuccess();
        }
        return ReturnResultUtils.returnFail(ReturnEnum.ShoppingCarEnum.DEL_ALL_FAIL);
    }

    /**
     * 订单支付
     *
     * @param orderId 订单id
     * @param current 当前用户
     * @return
     */
    @LoginRequired
    @ApiOperation(value = "订单支付")
    @PostMapping(value = "/payOrder")
    public ReturnResult payOrder(@ApiParam(value = "订单id") @RequestParam String orderId, @CurrentUser @ApiIgnore UserInfoVo current) {
        String code = null;
        try {
            code = orderWebService.payOrder(orderId, current);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return StringUtils.isEmpty(code) ? ReturnResultUtils.returnFail(ReturnEnum.OrderEnum.ERROR) :
                ReturnResultUtils.returnSuccess(code);
    }

}
