package com.kgc.cn.controller;

import com.kgc.cn.common.enums.ReturnEnum;
import com.kgc.cn.common.model.param.UserInfoParam;
import com.kgc.cn.common.utils.ReturnResult;
import com.kgc.cn.common.utils.result.ReturnResultUtils;
import com.kgc.cn.webservice.UserWebService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by scy on 2019/12/23
 */

@RestController
@RequestMapping
@Api(tags = "用户管理")
public class UserController {
    @Autowired
    private UserWebService userWebService;

    /**
     * 用户注册
     *
     * @param userInfoParam
     * @return
     */

    @PostMapping(value = "/registered")
    @ApiOperation("用户注册")
    public ReturnResult registered(UserInfoParam userInfoParam) {
        int num = userWebService.registered(userInfoParam);
        if (num == 0) {
            return ReturnResultUtils.returnFail(ReturnEnum.UserEnum.PHMA);
        }
        if (num == 2) {
            return ReturnResultUtils.returnFail(ReturnEnum.UserEnum.PHONE);
        }
        if (num == 3) {
            return ReturnResultUtils.returnFail(ReturnEnum.UserEnum.MAIL);
        }
        return ReturnResultUtils.returnSuccess();
    }

    /**
     * 登录
     *
     * @param account      手机号或者邮箱
     * @param userPassword 密码
     * @param request
     * @return
     */
    @PostMapping(value = "/login")
    @ApiOperation("用户登录")
    public ReturnResult login(@ApiParam(value = "邮箱或手机号", required = true) @RequestParam String account,
                              @ApiParam(value = "密码") @RequestParam String userPassword,
                              HttpServletRequest request) {
        String token = userWebService.Login(account, userPassword, request);
        if ("-1".equals(token)) {
            return ReturnResultUtils.returnFail(ReturnEnum.EmployeeAccountEnum.ERROR);
        }
        if ("2".equals(token)) {
            return ReturnResultUtils.returnFail(ReturnEnum.EmployeeAccountEnum.OLOGIN);
        }
        return ReturnResultUtils.returnSuccess(token);
    }


}
