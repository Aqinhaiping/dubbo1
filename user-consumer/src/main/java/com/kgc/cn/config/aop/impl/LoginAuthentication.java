package com.kgc.cn.config.aop.impl;

import com.kgc.cn.common.utils.exception.UserNotLoginException;
import com.kgc.cn.config.aop.LoginRequired;
import com.kgc.cn.utils.redis.RedisUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;

public class LoginAuthentication implements HandlerInterceptor {

    @Autowired
    private RedisUtils redisUtils;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        //判断是不是controller层的方法
        if (!(handler instanceof HandlerMethod)) {
            return true;
        }

        //判断方法有没有@LoginRequired注解
        HandlerMethod handlerMethod = (HandlerMethod) handler;
        Method method = handlerMethod.getMethod();
        LoginRequired loginRequired = method.getAnnotation(LoginRequired.class);

        if (loginRequired != null) {
            String token = request.getHeader("token");
            String userJsonStr = (String) redisUtils.get(token);
            if (!StringUtils.isEmpty(userJsonStr)) {
                request.setAttribute("userJsonStr", userJsonStr);
            } else {
                throw new UserNotLoginException();
            }
            return true;
        }

        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

    }
}
