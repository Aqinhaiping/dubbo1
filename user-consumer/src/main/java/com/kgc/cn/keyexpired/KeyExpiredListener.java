package com.kgc.cn.keyexpired;

import com.alibaba.dubbo.common.utils.CollectionUtils;
import com.alibaba.dubbo.config.annotation.Reference;
import com.google.common.collect.Maps;
import com.kgc.cn.common.enums.MailTemplates;
import com.kgc.cn.common.enums.NameSpaceEnum;
import com.kgc.cn.common.model.param.mail.MailParam;
import com.kgc.cn.common.model.vo.UserInfoVo;
import com.kgc.cn.common.service.third.MailService;
import com.kgc.cn.common.service.user.UserService;
import com.kgc.cn.common.utils.time.TimeUtils;
import com.kgc.cn.utils.redis.RedisUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.listener.KeyExpirationEventMessageListener;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.scheduling.annotation.Async;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by scy on 2019/12/24
 */

public class KeyExpiredListener extends KeyExpirationEventMessageListener {

    @Reference
    MailService mailService;

    @Reference
    UserService userService;

    @Autowired
    RedisUtils redisUtils;

    public KeyExpiredListener(RedisMessageListenerContainer listenerContainer) {
        super(listenerContainer);
    }

    @Override
    @Async
    public void onMessage(Message message, byte[] pattern) {
        String key = message.toString();
        String[] infos = key.split(":");
        if (NameSpaceEnum.VIP.getNamespace().equals(infos[0] + ":")) {
            UserInfoVo userInfoVo = new UserInfoVo();
            userInfoVo.setUserId(infos[1]);
            List<UserInfoVo> userInfoList = userService.select(userInfoVo);
            if (CollectionUtils.isNotEmpty(userInfoList) && null != userInfoList.get(0).getUserEmail()) {
                UserInfoVo user = userInfoList.get(0);
                Map<String, Object> params = Maps.newHashMap();
                params.put("username", user.getNickname());
                params.put("date", TimeUtils.getNextDayString(new Date(), 0));
                MailParam mailParam = MailParam.builder().to(user.getUserEmail()).subject("会员到期").templateParams(params).build();
                mailService.sendTemplateMail(mailParam, MailTemplates.VIP);
            }
        }
    }
}
