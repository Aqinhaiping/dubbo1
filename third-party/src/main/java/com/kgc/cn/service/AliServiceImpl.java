package com.kgc.cn.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.kgc.cn.common.model.param.SmsParam;
import com.kgc.cn.common.service.third.AliService;
import com.kgc.cn.utils.ali.AliUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;

/**
 * Created by scy on 2019/12/21
 */

@Service
public class AliServiceImpl implements AliService {
    @Autowired
    AliUtils aliUtils;

    @Override
    @Async
    public void sendSms(SmsParam smsParam) {
        try {
            String data = aliUtils.sendSms(smsParam);
            System.out.println(data);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
