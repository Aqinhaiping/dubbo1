package com.kgc.cn.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.kgc.cn.common.enums.MailTemplates;
import com.kgc.cn.common.model.param.mail.MailParam;
import com.kgc.cn.common.service.third.MailService;
import com.kgc.cn.utils.mail.MailUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;

/**
 * Created by scy on 2019/12/23
 */

@Service
public class MailServiceImpl implements MailService {
    @Autowired
    MailUtils mailUtils;

    @Override
    @Async
    public void sendTemplateMail(MailParam mailParam, MailTemplates mailTemplates) {
        try {
            mailUtils.sendTemplateMail(mailParam, mailTemplates.getTemplateName());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
