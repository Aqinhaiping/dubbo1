package com.kgc.cn.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.alibaba.fastjson.JSONObject;
import com.kgc.cn.common.model.dto.OrderDetail;
import com.kgc.cn.common.service.third.WxService;
import com.kgc.cn.common.utils.client.UrlUtils;
import com.kgc.cn.utils.OrderParam;
import com.kgc.cn.utils.WxPayParam;
import com.kgc.cn.utils.url.WxPayUrl;
import com.kgc.cn.utils.wx.WxUtils;
import com.kgc.cn.utils.xmlUrl.WxPayXmlUtil;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;

/**
 * Created by scy on 2019/12/21
 */

@Service
public class WxServiceImpl implements WxService {
    @Autowired
    WxPayUrl wxPayUrl;
    @Autowired
    WxUtils wxUtils;
    @Autowired
    WxPayParam wxPayParam;


    @Override
    public String getUserInfo(String code) {
        String jsonStr = UrlUtils.loadURL(wxUtils.reqAccessToken(code));
        JSONObject jsonObject = JSONObject.parseObject(jsonStr);
        String accessToken = jsonObject.getString("access_token");
        String openId = jsonObject.getString("openid");
        String userInfoJsonStr = UrlUtils.loadURL(wxUtils.reqUserInfo(accessToken, openId));
        return userInfoJsonStr;
    }

    @Override
    public Map<String, String> pay(OrderDetail orderDetail) throws Exception {
        OrderParam order = new OrderParam();
        order.setName(orderDetail.getProductId());
        order.setId(orderDetail.getOrderId());
        order.setPrice(1);
        return WxPayXmlUtil.xmlToMap(wxPayUrl.wxPay(order));
    }


    @Override
    public boolean isSignatureValid(String xml) throws Exception {
        Map<String, String> resultXml = WxPayXmlUtil.xmlToMap(xml);
        return WxPayXmlUtil.isSignatureValid(resultXml, wxPayParam.getKey());
    }

    @Override
    public String toLogin(String state) throws Exception {
        return wxUtils.reqCodeByState(state);
    }

    @Override
    public Map<String, String> xmlToMap(String xml) throws Exception {
        return WxPayXmlUtil.xmlToMap(xml);
    }
}
