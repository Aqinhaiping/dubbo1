package com.kgc.cn.utils.ali;

import com.alibaba.fastjson.JSONObject;
import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
import com.kgc.cn.common.model.param.SmsParam;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Created by scy on 2019/12/21
 */
@Component
@ConfigurationProperties(prefix = "ali-sms")
@Data
public class AliUtils {
    private String accessKeyId;
    private String accessSecret;
    private String signName;

    public String sendSms(SmsParam smsParam) throws Exception {
        DefaultProfile profile = DefaultProfile.getProfile("cn-hangzhou", accessKeyId, accessSecret);
        IAcsClient client = new DefaultAcsClient(profile);
        CommonRequest request = new CommonRequest();
        request.setMethod(MethodType.POST);
        request.setDomain("dysmsapi.aliyuncs.com");
        request.setVersion("2017-05-25");
        request.setAction("SendSms");
        request.putQueryParameter("RegionId", "cn-hangzhou");
        request.putQueryParameter("PhoneNumbers", smsParam.getToPhone());
        request.putQueryParameter("SignName", signName);
        request.putQueryParameter("TemplateCode", smsParam.getTemplateCode());
        request.putQueryParameter("TemplateParam", JSONObject.toJSONString(smsParam.getTemplateParams()));
        CommonResponse response = client.getCommonResponse(request);
        return response.getData();

    }


}
