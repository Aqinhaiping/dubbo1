package com.kgc.cn.utils;

import lombok.Data;

/**
 * Created by scy on 2019/12/10
 */

@Data
public class OrderParam {
    private String id;
    private String name;
    private int price;
}
