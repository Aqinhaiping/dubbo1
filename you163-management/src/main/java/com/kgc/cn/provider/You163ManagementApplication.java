package com.kgc.cn.provider;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class You163ManagementApplication {

    public static void main(String[] args) {
        SpringApplication.run(You163ManagementApplication.class, args);
    }

}
