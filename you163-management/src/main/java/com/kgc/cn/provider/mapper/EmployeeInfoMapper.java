package com.kgc.cn.provider.mapper;

import com.kgc.cn.common.model.dto.EmployeeInfo;
import com.kgc.cn.common.model.dto.example.EmployeeInfoExample;
import com.kgc.cn.common.model.param.EmployeeCheckOut;
import com.kgc.cn.common.model.param.EmployeeParam;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

public interface EmployeeInfoMapper {
    long countByExample(EmployeeInfoExample example);

    int deleteByExample(EmployeeInfoExample example);

    int deleteByPrimaryKey(String employeeId);

    int insert(EmployeeInfo record);

    int insertSelective(EmployeeInfo record);

    List<EmployeeInfo> selectByExample(EmployeeInfoExample example);

    EmployeeInfo selectByPrimaryKey(String employeeId);

    int updateByExampleSelective(@Param("record") EmployeeInfo record, @Param("example") EmployeeInfoExample example);

    int updateByExample(@Param("record") EmployeeInfo record, @Param("example") EmployeeInfoExample example);

    int updateByPrimaryKeySelective(EmployeeInfo record);

    int updateByPrimaryKey(EmployeeInfo record);

    /**
     * 员工登录
     *
     * @param phone    手机号
     * @param password 密码
     * @return 员工信息
     */
    EmployeeInfo toLogin(@Param("phone") String phone, @Param("password") String password);

    /**
     * 通过员工编号查询离职状态
     *
     * @param employeeId 员工编号
     * @return 员工信息
     */
    EmployeeInfo queryEmployeeInfo(@Param("employeeId") String employeeId);


    /**
     * 人员查询（模糊）
     *
     * @param record 员工信息
     * @return list集合
     */
    List<EmployeeInfo> queryEmployee(@Param("record") EmployeeParam record);


    List<EmployeeCheckOut> checkOut();

    /**
     * 员工出勤查询
     *
     * @param employeeId
     * @param attendanceTime
     * @return 员工信息集合
     */
    List<EmployeeInfo> queryEmployeeAttendance(@Param("employeeId") String employeeId, @Param("attendanceTime") Date attendanceTime);
}