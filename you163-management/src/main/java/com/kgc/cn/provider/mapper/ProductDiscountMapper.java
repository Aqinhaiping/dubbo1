package com.kgc.cn.provider.mapper;

import com.kgc.cn.common.model.dto.ProductDiscount;
import com.kgc.cn.common.model.dto.example.ProductDiscountExample;
import com.kgc.cn.common.model.param.ProductDetailsParam;
import com.kgc.cn.common.model.param.ProductDiscountsParam;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ProductDiscountMapper {
    long countByExample(ProductDiscountExample example);

    int deleteByExample(ProductDiscountExample example);

    int insert(ProductDiscount record);

    int insertSelective(ProductDiscount record);

    List<ProductDiscount> selectByExample(ProductDiscountExample example);

    int updateByExampleSelective(@Param("record") ProductDiscount record, @Param("example") ProductDiscountExample example);

    int updateByExample(@Param("record") ProductDiscount record, @Param("example") ProductDiscountExample example);

    List<ProductDiscountsParam> queryProductDiscount();

    /**
     * 商品详情展示
     *
     * @param productId
     * @return
     */
    ProductDetailsParam queryProductInfo(String productId);
}