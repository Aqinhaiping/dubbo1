package com.kgc.cn.provider.mapper;

import com.kgc.cn.common.model.dto.EmployeeDepartment;
import com.kgc.cn.common.model.dto.example.EmployeeDepartmentExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface EmployeeDepartmentMapper {
    long countByExample(EmployeeDepartmentExample example);

    int deleteByExample(EmployeeDepartmentExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(EmployeeDepartment record);

    int insertSelective(EmployeeDepartment record);

    List<EmployeeDepartment> selectByExample(EmployeeDepartmentExample example);

    EmployeeDepartment selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") EmployeeDepartment record, @Param("example") EmployeeDepartmentExample example);

    int updateByExample(@Param("record") EmployeeDepartment record, @Param("example") EmployeeDepartmentExample example);

    int updateByPrimaryKeySelective(EmployeeDepartment record);

    int updateByPrimaryKey(EmployeeDepartment record);
}