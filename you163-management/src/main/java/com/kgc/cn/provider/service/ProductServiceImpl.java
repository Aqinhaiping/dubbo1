package com.kgc.cn.provider.service;

import com.alibaba.dubbo.common.utils.CollectionUtils;
import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.kgc.cn.common.model.dto.ProductCategory;
import com.kgc.cn.common.model.dto.ProductDiscount;
import com.kgc.cn.common.model.dto.ProductInfo;
import com.kgc.cn.common.model.dto.example.ProductCategoryExample;
import com.kgc.cn.common.model.dto.example.ProductDiscountExample;
import com.kgc.cn.common.model.dto.example.ProductInfoExample;
import com.kgc.cn.common.model.param.ProductCheckOut;
import com.kgc.cn.common.model.param.ProductDetailsParam;
import com.kgc.cn.common.model.param.ProductDiscountsParam;
import com.kgc.cn.common.model.param.ProductInfoParam;
import com.kgc.cn.common.model.vo.ProductDiscountVo;
import com.kgc.cn.common.model.vo.ProductInfoVo;
import com.kgc.cn.common.service.management.ProductService;
import com.kgc.cn.common.utils.copy.CopyUtils;
import com.kgc.cn.provider.mapper.ProductCategoryMapper;
import com.kgc.cn.provider.mapper.ProductDiscountMapper;
import com.kgc.cn.provider.mapper.ProductInfoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * Created by scy on 2019/12/10
 */

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductInfoMapper productInfoMapper;
    @Autowired
    private ProductDiscountMapper discountMapper;
    @Autowired
    private ProductCategoryMapper categoryMapper;


    @Override
    public int addProductInfo(ProductInfoVo productInfoVo) {
        ProductInfo productInfo = CopyUtils.copy(productInfoVo, ProductInfo.class);
        return productInfoMapper.insertSelective(productInfo);
    }

    @Override
    public int updateProduct(ProductInfoVo productInfoVo) {
        ProductInfo productInfo = CopyUtils.copy(productInfoVo, ProductInfo.class);
        return productInfoMapper.updateByPrimaryKeySelective(productInfo);
    }

    @Override
    public List<ProductInfoVo> show(ProductInfoParam productInfoParam) {
        List<ProductInfo> productInfos = productInfoMapper.show(productInfoParam);
        List<ProductInfoVo> params = CopyUtils.copyList(productInfos, ProductInfoVo.class);
        return params;
    }

    @Override
    public int deleteProduct(String productId) {
        return productInfoMapper.deleteByPrimaryKey(productId);
    }


    @Override
    public List<ProductCheckOut> checkOut() {
        return productInfoMapper.checkOut();
    }

    @Override
    @Transactional
    public int discount(ProductDiscountVo discountVo) {
        ProductInfoExample infoExample = new ProductInfoExample();
        infoExample.createCriteria().andProductIdEqualTo(discountVo.getProductId());
        //判断商品是否存在
        if (CollectionUtils.isNotEmpty(productInfoMapper.selectByExample(infoExample))) {
            ProductDiscountExample discountExample = new ProductDiscountExample();
            discountExample.createCriteria().andProductIdEqualTo(discountVo.getProductId());
            ProductDiscount productDiscount = CopyUtils.copy(discountVo, ProductDiscount.class);
            //判断商品是否已打折（是->更新）
            if (CollectionUtils.isNotEmpty(discountMapper.selectByExample(discountExample))) {
                return discountMapper.updateByExampleSelective(productDiscount, discountExample);
            }
            return discountMapper.insert(productDiscount);
        }
        return 0;
    }

    @Override
    public PageInfo<ProductInfoVo> paging(ProductInfoParam productInfoParam, int page, int size) {
        PageInfo<ProductInfoVo> pageInfo = PageHelper.startPage(page, size).doSelectPageInfo(() -> show(productInfoParam));
        return pageInfo;
    }


    @Override
    public List<ProductDiscountsParam> queryProductDiscount() {
        return discountMapper.queryProductDiscount();
    }

    @Override
    @Transactional
    public List<Map<String, Map<String, Integer>>> queryCount() {
        List<ProductCategory> productCategoryList = categoryMapper.selectByExample(new ProductCategoryExample());
        List<Map<String, Map<String, Integer>>> arrayList = Lists.newArrayList();
        productCategoryList.forEach(param -> {
            Map<String, Map<String, Integer>> Cmap = Maps.newHashMap();
            ProductInfoExample infoExample = new ProductInfoExample();
            infoExample.createCriteria().andCategoryIdEqualTo(String.valueOf(param.getCategoryId()));
            List<ProductInfo> productInfos = productInfoMapper.selectByExample(infoExample);
            Map<String, Integer> Pmap = Maps.newHashMap();
            productInfos.forEach(p -> {
                Pmap.put(p.getProductName(), p.getProductStock());
            });
            Cmap.put(param.getCategoryName(), Pmap);
            arrayList.add(Cmap);
        });
        return arrayList;
    }

    @Override
    public List<Map<String, Map<String, List<ProductInfo>>>> merchandise() {
        List<ProductCategory> productCategoryList = categoryMapper.selectByExample(new ProductCategoryExample());
        List<Map<String, Map<String, List<ProductInfo>>>> list = Lists.newArrayList();
        productCategoryList.forEach(Param -> {
            Map<String, Map<String, List<ProductInfo>>> Cmap = Maps.newHashMap();
            ProductInfoExample infoExample = new ProductInfoExample();
            infoExample.createCriteria().andCategoryIdEqualTo(String.valueOf(Param.getCategoryId()));
            List<ProductInfo> productInfoList = productInfoMapper.selectByExample(infoExample);
            Map<String, List<ProductInfo>> Pmap = Maps.newHashMap();
            productInfoList.forEach(p -> {
                List<ProductInfo> productInfos = productInfoMapper.select(p.getProductName());
                Pmap.put(p.getProductName(), productInfos);
            });
            Cmap.put(Param.getCategoryName(), Pmap);
            list.add(Cmap);
        });
        return list;
    }

    @Override
    public ProductDetailsParam queryProductInfo(String productId) {
        return discountMapper.queryProductInfo(productId);
    }
}
