package com.kgc.cn.provider.mapper;

import com.kgc.cn.common.model.dto.ProductInfo;
import com.kgc.cn.common.model.dto.example.ProductInfoExample;
import com.kgc.cn.common.model.param.ProductCheckOut;
import com.kgc.cn.common.model.param.ProductInfoParam;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ProductInfoMapper {
    long countByExample(ProductInfoExample example);

    int deleteByExample(ProductInfoExample example);

    int deleteByPrimaryKey(String productId);

    int insert(ProductInfo record);

    int insertSelective(ProductInfo record);

    List<ProductInfo> selectByExample(ProductInfoExample example);

    ProductInfo selectByPrimaryKey(String productId);

    int updateByExampleSelective(@Param("record") ProductInfo record, @Param("example") ProductInfoExample example);

    int updateByExample(@Param("record") ProductInfo record, @Param("example") ProductInfoExample example);

    int updateByPrimaryKeySelective(ProductInfo record);

    int updateByPrimaryKey(ProductInfo record);

    List<ProductInfo> show(@Param("record") ProductInfoParam record);

    List<ProductCheckOut> checkOut();

    List<ProductInfo> select(String productName);
}