package com.kgc.cn.provider.mapper;

import com.kgc.cn.common.model.dto.EmployeeAttendance;
import com.kgc.cn.common.model.dto.example.EmployeeAttendanceExample;
import com.kgc.cn.common.model.param.AttendanceParam;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface EmployeeAttendanceMapper {
    long countByExample(EmployeeAttendanceExample example);

    int deleteByExample(EmployeeAttendanceExample example);

    int deleteByPrimaryKey(Long attendanceId);

    int insert(EmployeeAttendance record);

    int insertSelective(EmployeeAttendance record);

    List<EmployeeAttendance> selectByExample(EmployeeAttendanceExample example);

    EmployeeAttendance selectByPrimaryKey(Long attendanceId);

    int updateByExampleSelective(@Param("record") EmployeeAttendance record, @Param("example") EmployeeAttendanceExample example);

    int updateByExample(@Param("record") EmployeeAttendance record, @Param("example") EmployeeAttendanceExample example);

    int updateByPrimaryKeySelective(EmployeeAttendance record);

    int updateByPrimaryKey(EmployeeAttendance record);

    List<AttendanceParam> attendance();

    List<AttendanceParam> disAttendance();

}