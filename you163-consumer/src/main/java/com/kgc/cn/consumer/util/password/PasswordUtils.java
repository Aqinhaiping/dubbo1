package com.kgc.cn.consumer.util.password;

import java.util.UUID;

/**
 * Created by scy on 2019/12/19
 */


public class PasswordUtils {

    public static String createPassword() {
        return UUID.randomUUID().toString().substring(0, 8);

    }
}
