package com.kgc.cn.consumer.config.annotation.resolver;

import com.alibaba.fastjson.JSONObject;
import com.kgc.cn.common.model.vo.EmployeeInfoVo;
import com.kgc.cn.consumer.config.annotation.CurrentEmployee;
import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

/**
 * Created by boot on 2019/11/28.
 */
public class CurrentEmployeeMsg implements HandlerMethodArgumentResolver {
    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        return parameter.getParameterType().isAssignableFrom(EmployeeInfoVo.class) &&
                parameter.hasParameterAnnotation(CurrentEmployee.class);
    }

    @Override
    public Object resolveArgument(MethodParameter methodParameter, ModelAndViewContainer modelAndViewContainer, NativeWebRequest nativeWebRequest, WebDataBinderFactory webDataBinderFactory) throws Exception {
        String jsonStr = (String) nativeWebRequest.getAttribute("employeeJsonStr", RequestAttributes.SCOPE_REQUEST);
        EmployeeInfoVo employeeInfoVo = JSONObject.parseObject(jsonStr, EmployeeInfoVo.class);

        return employeeInfoVo;
    }
}
