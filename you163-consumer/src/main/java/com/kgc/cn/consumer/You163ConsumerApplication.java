package com.kgc.cn.consumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
public class You163ConsumerApplication {

    public static void main(String[] args) {
        SpringApplication.run(You163ConsumerApplication.class, args);
    }

}
