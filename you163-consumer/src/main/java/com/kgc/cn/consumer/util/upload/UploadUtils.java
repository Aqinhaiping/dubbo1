package com.kgc.cn.consumer.util.upload;

/**
 * Created by scy on 2019/12/12
 */


import com.google.common.collect.Lists;
import com.kgc.cn.common.utils.time.TimeUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.List;

public class UploadUtils {

    private static String uploadPath = "/Users/xulei/分布式微服笔记";
    // private static String uploadPath = "C:/Temp/";

    //单文件上传
    public static File upload(MultipartFile file) {
        BufferedOutputStream stream = null;
        try {
            if (file.isEmpty()) {
                return null;
            }
            // 获取文件名
            String fileName = file.getOriginalFilename();
            String prefix = fileName.substring(0, fileName.lastIndexOf("."));
            String suffix = fileName.substring(fileName.lastIndexOf("."));
            fileName = prefix + TimeUtils.getCurrentTime() + suffix;
            File dest = new File(uploadPath + fileName);
            // 检测是否存在目录
            if (!dest.getParentFile().exists()) {
                dest.getParentFile().mkdirs();
            }
            file.transferTo(dest);
            return dest;
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static List<File> handleFileUpload(HttpServletRequest request) {
        List<MultipartFile> files = ((MultipartHttpServletRequest) request).getFiles("file");
        MultipartFile file = null;
        List<File> allFile = Lists.newArrayList();
        for (int i = 0; i < files.size(); ++i) {
            file = files.get(i);
            if (!file.isEmpty()) {
                try {
                    String fileName = file.getOriginalFilename();
                    File dest = new File(uploadPath + fileName);
                    file.transferTo(dest);
                    allFile.add(dest);
                } catch (Exception e) {
                    return null;
                }
            } else {
                return null;
            }
        }
        return allFile;
    }
}
