package com.kgc.cn.consumer.param.excel;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by scy on 2019/12/17
 */

@Data
public class DiscountExcelParam extends BaseRowModel implements Serializable {

    private static final long serialVersionUID = -6022151892715874960L;

    @ExcelProperty(value = "商品id", index = 0)
    private String productId;
    @ExcelProperty(value = "折扣等级", index = 1)
    private Integer discountLevel;
    @ExcelProperty(value = "开始时间", index = 2)
    private Date startTime;
    @ExcelProperty(value = "结束时间", index = 3)
    private Date endTime;

}
