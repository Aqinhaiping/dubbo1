package com.kgc.cn.consumer.webService;

/**
 * Created by scy on 2019/12/16
 */


public interface FileWebService {
    /**
     * 员工信息导出
     *
     * @param path 导出路径
     */
    void checkOutEmployee(String path);

    /**
     * 商品信息导出
     *
     * @param path 导出路径
     */
    void checkOutProduct(String path);
}
