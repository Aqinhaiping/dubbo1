package com.kgc.cn.consumer.config.annotation.interceptor;

import com.alibaba.dubbo.common.utils.StringUtils;
import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSONObject;
import com.kgc.cn.common.model.vo.EmployeeInfoVo;
import com.kgc.cn.common.service.management.PowerService;
import com.kgc.cn.common.utils.exception.LimitedPowerException;
import com.kgc.cn.common.utils.exception.UserNotLoginException;
import com.kgc.cn.consumer.config.annotation.PowerRequired;
import com.kgc.cn.consumer.util.redis.RedisUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;

/**
 * Created by boot on 2019/11/28.
 */
public class PowerAuthentication implements HandlerInterceptor {
    @Autowired
    private RedisUtils redisUtils;

    @Reference
    PowerService powerService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse httpServletResponse, Object handler) throws Exception {
        if (!(handler instanceof HandlerMethod)) {
            return true;
        }
        HandlerMethod handlerMethod = (HandlerMethod) handler;
        Method method = handlerMethod.getMethod();

        PowerRequired powerRequired = method.getAnnotation(PowerRequired.class);
        if (powerRequired != null) {
            //令牌（登录令牌）
            String token = request.getHeader("token");
            if (StringUtils.isNotEmpty(token) && redisUtils.hasKey(token)) {
                String jsonStr = (String) redisUtils.get(token);
                EmployeeInfoVo employeeInfoVo = JSONObject.parseObject(jsonStr, EmployeeInfoVo.class);
                if ("1".equals(powerService.showAccountLevel(employeeInfoVo.getEmployeeId()))) {
                    request.setAttribute("employeeJsonStr", jsonStr);
                    return true;
                }
                // 账号等级验证
                if (powerRequired.level() < Integer.parseInt(powerService.showAccountLevel(employeeInfoVo.getEmployeeId()))) {
                    throw new LimitedPowerException();
                }
                // 部门验证
                if (!StringUtils.isEmpty(powerRequired.departmentId()) && !powerRequired.departmentId().equals(employeeInfoVo.getDepartmentId())) {
                    throw new LimitedPowerException();
                }
                request.setAttribute("employeeJsonStr", jsonStr);
            } else {
                throw new UserNotLoginException();
            }

            return true;
        }
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView) throws Exception {
    }

    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) throws Exception {
    }
}
