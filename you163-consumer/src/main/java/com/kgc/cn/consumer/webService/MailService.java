package com.kgc.cn.consumer.webService;

import com.kgc.cn.consumer.param.mail.MailParam;

/**
 * Created by scy on 2019/12/12
 */
public interface MailService {

    /**
     * 对新添加的员工发送通知邮件
     *
     * @param mailParam 邮件参数
     */
    void sendAccountMails(MailParam mailParam);

    /**
     * 发送重置密码邮件
     *
     * @param mailParam 邮件参数
     */
    void sendResetPasswordMail(MailParam mailParam);
}
