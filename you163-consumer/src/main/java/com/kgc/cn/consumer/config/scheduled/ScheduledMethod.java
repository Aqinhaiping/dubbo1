package com.kgc.cn.consumer.config.scheduled;

import com.kgc.cn.consumer.util.redis.RedisUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Set;

/**
 * Created by scy on 2019/12/17
 */

@Component
public class ScheduledMethod {

    @Autowired
    private RedisUtils redisUtils;

    private String attendanceNameSpace = "employeeAttendance:";

    @Scheduled(cron = "0 0 0 * * ? ")
    //@Scheduled(cron = " * * * * * ?")
    public void clearAttendance() {
        Set<String> keys = redisUtils.keys(attendanceNameSpace + "*");
        redisUtils.del(keys.toArray(new String[keys.size()]));
    }
}
