package com.kgc.cn.consumer.webService.impl;

import com.alibaba.dubbo.common.utils.CollectionUtils;
import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.google.common.collect.Lists;
import com.kgc.cn.common.model.param.AttendanceParam;
import com.kgc.cn.common.model.param.EmployeeInfoParam;
import com.kgc.cn.common.model.param.EmployeeParam;
import com.kgc.cn.common.model.vo.EmployeeInfoVo;
import com.kgc.cn.common.service.management.EmployeeService;
import com.kgc.cn.common.service.management.PowerService;
import com.kgc.cn.common.utils.copy.CopyUtils;
import com.kgc.cn.consumer.param.excel.EmployeeExcelParam;
import com.kgc.cn.consumer.util.redis.RedisUtils;
import com.kgc.cn.consumer.webService.EmployeeWebService;
import com.kgc.cn.consumer.webService.activemq.ActiveMqServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Objects;

/**
 * Created by scy on 2019/12/10
 */

@Service
public class EmployeeWebServiceImpl implements EmployeeWebService {

    @Reference
    private EmployeeService employeeService;

    @Autowired
    ActiveMqServer activeMqServer;

    @Reference
    private PowerService powerService;

    @Autowired
    private RedisUtils redisUtils;

    private String employeeAttendanceNameSpace = "employeeAttendance:";


    @Override
    @Transactional
    public void addEmployee(List<EmployeeExcelParam> list) {
        List<EmployeeInfoVo> infoList = Lists.newArrayList();
        list.forEach(param -> {
            EmployeeInfoVo employeeInfoVo = CopyUtils.copy(param, EmployeeInfoVo.class).createId();
            infoList.add(employeeInfoVo);
            employeeService.addEmployee(employeeInfoVo);
        });
        activeMqServer.sendAccountMail(infoList);
    }


    @Override
    @Transactional
    public String toLogin(String phone, String password, HttpServletRequest request) {
        EmployeeInfoVo employeeInfoVo = employeeService.toLogin(phone, password);
        if (Objects.isNull(employeeInfoVo)) {
            return "1";
        }
        if (employeeInfoVo.getIsDimission() == 1) {
            return "2";
        }
        String json = JSONObject.toJSONString(employeeInfoVo);
        String token = request.getSession().getId();
        redisUtils.set(token, json, 1200);
        return token;
    }


    @Override
    @Transactional
    public int updateEmployeeInfo(EmployeeInfoParam employeeInfoParam, EmployeeInfoVo current) {
        EmployeeInfoVo employeeInfoVo = CopyUtils.copy(employeeInfoParam, EmployeeInfoVo.class);
        // 获取被修改者的权限
        int level = Integer.parseInt(powerService.showAccountLevel(employeeInfoVo.getEmployeeId()));
        // 获取当前登录人员的权限
        int currentLevel = Integer.parseInt(powerService.showAccountLevel(current.getEmployeeId()));
        if (currentLevel > level) {
            return 2;
        }
        return employeeService.updateEmployeeInfo(employeeInfoVo);
    }


    @Override
    @Transactional
    public int delEmployee(String employeeId, EmployeeInfoVo current) {
        EmployeeInfoVo employeeInfoVo = employeeService.queryEmployeeInfo(employeeId);
        // 获取删除人员的权限
        int level = Integer.parseInt(powerService.showAccountLevel(employeeInfoVo.getEmployeeId()));
        // 获取当前登录人员的权限
        int currentLevel = Integer.parseInt(powerService.showAccountLevel(current.getEmployeeId()));
        if (currentLevel > level) {
            return 1;
        }
        if (employeeInfoVo.getIsDimission() == 1) {
            return 2;
        }
        employeeService.delEmployee(employeeId);
        return 3;
    }


    @Override
    @Transactional
    public int resetPassword(String employeeId, EmployeeInfoVo current) {
        // 获取被修改者的权限
        int level = Integer.parseInt(powerService.showAccountLevel(employeeId));
        // 获取当前登录人员的权限
        int currentLevel = Integer.parseInt(powerService.showAccountLevel(current.getEmployeeId()));
        if (currentLevel > level) {
            return 2;
        }
        List<EmployeeInfoVo> infos = employeeService.queryEmployee(EmployeeParam.builder().employeeId(employeeId).build());
        if (CollectionUtils.isEmpty(infos)) {
            return 3;
        }
        activeMqServer.sendResetPasswordMail(infos.get(0), current);
        return 1;
    }


    @Override
    @Transactional
    public Boolean loginOut(HttpServletRequest request) {
        // 获取token的value
        String token = request.getHeader("token");
        if (!redisUtils.hasKey(token)) {
            return false;
        }
        redisUtils.del(token);
        return true;
    }


    @Override
    @Transactional
    public int addEmployeeAttendance(HttpServletRequest request, EmployeeInfoVo current) {
        String key = employeeAttendanceNameSpace + current.getEmployeeId();
        if (redisUtils.hasKey(key)) {
            return 1;
        }
        redisUtils.set(key, "");
        employeeService.addEmployeeAttendance(current.getEmployeeId());
        return 2;
    }


    @Override
    public PageInfo<AttendanceParam> queryAttendance(boolean flag, int page, int size) {
        return employeeService.queryAttendance(flag, page, size);
    }

    @Override
    public PageInfo<EmployeeInfoVo> paging(EmployeeParam employeeParam, int page, int size) {
        return employeeService.paging(employeeParam, page, size);
    }


}
