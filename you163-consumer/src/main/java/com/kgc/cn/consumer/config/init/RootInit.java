package com.kgc.cn.consumer.config.init;

import com.alibaba.dubbo.config.annotation.Reference;
import com.kgc.cn.common.model.dto.EmployeeAccount;
import com.kgc.cn.common.model.vo.EmployeeInfoVo;
import com.kgc.cn.common.service.management.EmployeeService;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;

import java.util.Date;

/**
 * Created by scy on 2019/12/19
 */

//@Component
public class RootInit implements ApplicationRunner {


    @Reference
    private EmployeeService employeeService;

    //初始化root用户
    @Override
    public void run(ApplicationArguments args) {
        employeeService.addEmployee(EmployeeInfoVo.builder().employeeId("1000root")
                .employeeEmail("root").employeePhone("root").departmentId("root").entryTime(new Date())
                .employeeName("系统管理员").employeeGender("r").isDimission(0).roleId(0).build());
        employeeService.addEmployeeAccount(EmployeeAccount.builder().account("root").password("root").employeeId("1000root").level("1").build());
    }
}
