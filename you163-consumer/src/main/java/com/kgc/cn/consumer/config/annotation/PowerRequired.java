package com.kgc.cn.consumer.config.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by boot on 2019/11/28.
 */
@Target(ElementType.METHOD)//只能用在方法之上
@Retention(RetentionPolicy.RUNTIME)//运行时有效
public @interface PowerRequired {

    int level() default 3;

    String departmentId() default "";
}