package com.kgc.cn.consumer.param;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@ApiModel
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class EmployeeAccountParam implements Serializable {
    private static final long serialVersionUID = 2965488359639420775L;

    @ApiModelProperty(value = "员工编号")
    private String employeeId;
    @ApiModelProperty(value = "员工账号")
    private String account;
    @ApiModelProperty(value = "员工密码")
    private String password;
    @ApiModelProperty(value = "员工权限")
    private String level;


}