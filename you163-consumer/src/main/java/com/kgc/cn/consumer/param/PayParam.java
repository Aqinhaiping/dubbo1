package com.kgc.cn.consumer.param;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * Created by scy on 2019/12/7
 */
@ApiModel("支付信息")
@Data
public class PayParam {
    @ApiModelProperty("订单id")
    private String OrderId;
    @ApiModelProperty("是否成功支付")
    private boolean isPay;
}
