package com.kgc.cn.consumer.controller;

import com.alibaba.dubbo.common.utils.CollectionUtils;
import com.github.pagehelper.PageInfo;
import com.google.common.collect.Lists;
import com.kgc.cn.common.enums.ReturnEnum;
import com.kgc.cn.common.model.param.EmployeeInfoParam;
import com.kgc.cn.common.model.param.EmployeeParam;
import com.kgc.cn.common.model.vo.EmployeeInfoVo;
import com.kgc.cn.common.utils.ReturnResult;
import com.kgc.cn.common.utils.result.ReturnResultUtils;
import com.kgc.cn.consumer.config.annotation.CurrentEmployee;
import com.kgc.cn.consumer.config.annotation.PowerRequired;
import com.kgc.cn.consumer.param.excel.EmployeeExcelParam;
import com.kgc.cn.consumer.util.excel.EasyExcelUtils;
import com.kgc.cn.consumer.util.upload.UploadUtils;
import com.kgc.cn.consumer.webService.EmployeeWebService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.File;
import java.util.List;

/**
 * Created by scy on 2019/12/10
 */

@Api(tags = "员工管理")
@RestController
@RequestMapping(value = "employee")
public class EmployeeController {

    @Autowired
    private EmployeeWebService employeeWebService;

    /**
     * 录入员工（excel）
     *
     * @param webFile 需要读取的文件
     * @return 成功
     */
    @ApiOperation("录入员工（excel）")
    @PostMapping(value = "/addEmployees")
    @ApiImplicitParam(name = "sheetNames", value = "需要读取的表名", required = true, paramType = "query", allowMultiple = true, dataType = "String")
    @PowerRequired(departmentId = "3003")
    public ReturnResult addEmployees(@ApiParam(value = "需要读取的文件", required = true) MultipartFile webFile, String[] sheetNames) {
        File file = UploadUtils.upload(webFile);
        List<String> errorNames = Lists.newArrayList();
        for (String sheetName : sheetNames) {
            List<EmployeeExcelParam> excelParamsList = (List<EmployeeExcelParam>) EasyExcelUtils.readExcelByModelSheetName(file,
                    sheetName, EmployeeExcelParam.class);
            if (CollectionUtils.isEmpty(excelParamsList)) {
                errorNames.add(sheetName);
                continue;
            }
            employeeWebService.addEmployee(excelParamsList);
        }
        if (CollectionUtils.isEmpty(errorNames)) {
            return ReturnResultUtils.returnSuccess();
        } else {
            return ReturnResultUtils.returnFail(ReturnEnum.ExcelEnum.SHEET_NAME, errorNames);
        }
    }


    /**
     * 员工登录
     *
     * @param phone    手机号
     * @param password 密码
     * @param request  sessionId
     * @return 登录令牌
     */
    @ApiOperation(value = "员工登录")
    @PostMapping(value = "/loginEmployees")
    public ReturnResult loginEmployees(@ApiParam(value = "手机号", required = true) @RequestParam String phone,
                                       @ApiParam(value = "密码", required = true) @RequestParam String password,
                                       HttpServletRequest request) {
        String token = employeeWebService.toLogin(phone, password, request);
        if ("1".equals(token)) {
            return ReturnResultUtils.returnFail(ReturnEnum.EmployeeAccountEnum.ERROR);
        }
        if ("2".equals(token)) {
            return ReturnResultUtils.returnFail(ReturnEnum.EmployeeAccountEnum.NONE);
        }
        return ReturnResultUtils.returnSuccess(token);
    }

    /**
     * 员工查询
     *
     * @param employeeParam 员工信息
     * @return 返回分页
     */
    @ApiOperation(value = "员工查询")
    @PostMapping(value = "/showEmployees")
    @PowerRequired
    public ReturnResult showEmployees(@Valid EmployeeParam employeeParam,
                                      @ApiParam(value = "当前页", example = "1") @RequestParam int page,
                                      @ApiParam(value = "分页规格", example = "10") @RequestParam int size) {
        PageInfo<EmployeeInfoVo> pageInfo = employeeWebService.paging(employeeParam, page, size);
        return ReturnResultUtils.returnSuccess(pageInfo);
    }

    /**
     * 员工删除
     *
     * @param employeeId 员工编号
     * @param current    当前员工信息
     * @return 影响条数
     */
    @PowerRequired(level = 2, departmentId = "3003")
    @ApiOperation(value = "员工删除")
    @PostMapping(value = "/delEmployees")
    public ReturnResult delEmployees(String employeeId, @CurrentEmployee @ApiIgnore EmployeeInfoVo current) {
        int id = employeeWebService.delEmployee(employeeId, current);
        if (id == 1) {
            return ReturnResultUtils.returnFail(ReturnEnum.EmployeeAccountEnum.ROLE_FAIL);
        }
        if (id == 2) {
            return ReturnResultUtils.returnFail(ReturnEnum.EmployeeAccountEnum.DEL_FAIL);
        }
        return ReturnResultUtils.returnSuccess();
    }

    /**
     * 员工信息修改
     *
     * @param employeeInfoParam 员工信息
     * @param current           当前员工信息
     * @return 影响条数
     */
    @PowerRequired(departmentId = "3003")
    @ApiOperation(value = "员工信息修改")
    @PostMapping(value = "/updateEmployeeInfo")
    public ReturnResult updateEmployeeInfo(@Valid EmployeeInfoParam employeeInfoParam, @CurrentEmployee @ApiIgnore EmployeeInfoVo current) {
        int count = employeeWebService.updateEmployeeInfo(employeeInfoParam, current);
        if (count == 2) {
            return ReturnResultUtils.returnFail(ReturnEnum.EmployeeEnum.FAIL);
        }
        return ReturnResultUtils.returnSuccess();
    }

    /**
     * 密码重置
     *
     * @param employeeId 员工id
     * @param current    当前员工信息
     * @return 影响条数
     */
    @PowerRequired(level = 2)
    @ApiOperation(value = "密码重置")
    @PostMapping(value = "/resetPassword")
    public ReturnResult resetPassword(String employeeId, @CurrentEmployee @ApiIgnore EmployeeInfoVo current) {
        int count = employeeWebService.resetPassword(employeeId, current);
        if (count == 2) {
            return ReturnResultUtils.returnFail(ReturnEnum.EmployeeEnum.FAIL);
        }
        if (count == 3) {
            return ReturnResultUtils.returnFail(ReturnEnum.EmployeeEnum.NONE);
        }
        return ReturnResultUtils.returnSuccess();
    }


    /**
     * 打卡
     *
     * @param request sessionId
     * @param current 当前员工信息
     * @return 成功
     */
    @PowerRequired
    @ApiOperation(value = "员工打卡")
    @PostMapping(value = "/addEmployeeAttendance")
    public ReturnResult addEmployeeAttendance(HttpServletRequest request, @CurrentEmployee @ApiIgnore EmployeeInfoVo current) {
        int count = employeeWebService.addEmployeeAttendance(request, current);
        if (count == 1) {
            return ReturnResultUtils.returnFail(ReturnEnum.EmployeeAccountEnum.PUNCH_CARD_FAIL);
        }

        return ReturnResultUtils.returnSuccess();
    }

    /**
     * 注销登录
     *
     * @param request sessionId
     * @return 布尔类型
     */
    @PowerRequired
    @ApiOperation(value = "注销登录")
    @PostMapping(value = "/loginOut")
    public ReturnResult loginOut(HttpServletRequest request) {
        boolean loginOut = employeeWebService.loginOut(request);
        if (!loginOut) {
            return ReturnResultUtils.returnFail(ReturnEnum.EmployeeAccountEnum.LOGIN_FAIL);
        }
        return ReturnResultUtils.returnSuccess();
    }

    /**
     * 查询出勤
     *
     * @param flag true 已出勤 false 未出勤
     * @return 出勤或未出勤信息
     */
    @ApiOperation("查询出勤")
    @GetMapping(value = "/queryAttendance")
    public ReturnResult queryAttendance(
            @ApiParam(value = "是否出勤", required = true) @RequestParam boolean flag,
            @ApiParam(value = "当前页", example = "1", required = true) @RequestParam int page,
            @ApiParam(value = "分页规格", example = "10", required = true) @RequestParam int size) {
        return ReturnResultUtils.returnSuccess(employeeWebService.queryAttendance(flag, page, size));
    }

}
