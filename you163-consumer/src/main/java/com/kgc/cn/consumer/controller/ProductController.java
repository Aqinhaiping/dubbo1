package com.kgc.cn.consumer.controller;

import com.alibaba.excel.util.CollectionUtils;
import com.github.pagehelper.PageInfo;
import com.google.common.collect.Lists;
import com.kgc.cn.common.enums.ReturnEnum;
import com.kgc.cn.common.model.dto.ProductInfo;
import com.kgc.cn.common.model.param.ProductDiscountsParam;
import com.kgc.cn.common.model.param.ProductInfoParam;
import com.kgc.cn.common.model.param.ProductUpdateParam;
import com.kgc.cn.common.model.vo.ProductDiscountVo;
import com.kgc.cn.common.model.vo.ProductInfoVo;
import com.kgc.cn.common.utils.ReturnResult;
import com.kgc.cn.common.utils.result.ReturnResultUtils;
import com.kgc.cn.consumer.config.annotation.PowerRequired;
import com.kgc.cn.consumer.param.excel.DiscountExcelParam;
import com.kgc.cn.consumer.param.excel.ProductInfoExcelParam;
import com.kgc.cn.consumer.util.excel.EasyExcelUtils;
import com.kgc.cn.consumer.util.upload.UploadUtils;
import com.kgc.cn.consumer.webService.ProductWebService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.File;
import java.util.List;
import java.util.Map;

/**
 * Created by scy on 2019/12/10
 */
@Api(tags = "商品管理")
@RestController
@RequestMapping(value = "/product")
public class ProductController {

    @Autowired
    private ProductWebService productWebService;


    /**
     * 添加商品(excel)
     *
     * @param webFile 上传文件
     * @return 录入结果
     */
    @ApiOperation("添加商品(excel)")
    @PostMapping(value = "/addProductInfo")
    @PowerRequired(departmentId = "3002")
    @ApiImplicitParam(name = "sheetNames", value = "需要读取的表名", required = true, paramType = "query", allowMultiple = true, dataType = "String")
    public ReturnResult addProductInfo(@ApiParam(value = "需要读取的文件", required = true) MultipartFile webFile, String[] sheetNames) {
        File file = UploadUtils.upload(webFile);
        List<String> errorNames = Lists.newArrayList();
        for (String sheetName : sheetNames) {
            List<ProductInfoExcelParam> productInfoExcel = (List<ProductInfoExcelParam>) EasyExcelUtils.readExcelByModelSheetName(file,
                    sheetName, ProductInfoExcelParam.class);
            if (CollectionUtils.isEmpty(productInfoExcel)) {
                errorNames.add(sheetName);
                continue;
            }
            productWebService.addProductInfo(productInfoExcel);
        }
        if (CollectionUtils.isEmpty(errorNames)) {
            return ReturnResultUtils.returnSuccess();
        } else {
            return ReturnResultUtils.returnFail(ReturnEnum.ExcelEnum.SHEET_NAME, errorNames);
        }
    }

    /**
     * 修改商品
     *
     * @param productUpdateParam 商品参数
     * @return 影响条数
     */

    @ApiOperation("修改商品")
    @PostMapping(value = "updateProductInfo")
    @PowerRequired(departmentId = "3002")
    public ReturnResult updateProductInfo(@Valid ProductUpdateParam productUpdateParam) {
        productWebService.updateProductInfo(productUpdateParam);
        return ReturnResultUtils.returnSuccess();
    }

    /**
     * 查询商品(模糊查询)
     *
     * @param productInfoParam 商品参数
     * @param page             当前页码
     * @param size             分页规格
     * @return 商品信息
     */
    @ApiOperation("查询商品(模糊查询)")
    @PostMapping(value = "show")
    public ReturnResult show(@Valid ProductInfoParam productInfoParam,
                             @ApiParam(value = "当前页", example = "1") @RequestParam int page,
                             @ApiParam(value = "分页规格", example = "10") @RequestParam int size) {
        PageInfo<ProductInfoVo> pageInfo = productWebService.paging(productInfoParam, page, size);
        return ReturnResultUtils.returnSuccess(pageInfo);
    }

    /**
     * 添加折扣（单件）
     *
     * @param discountVo 折扣信息
     * @return 影响条数
     */
    @ApiOperation("添加折扣（单件）")
    @PowerRequired(departmentId = "3002")
    @PostMapping(value = "/discount")
    public ReturnResult discount(@Valid ProductDiscountVo discountVo) {
        return productWebService.discount(discountVo) ?
                ReturnResultUtils.returnSuccess() : ReturnResultUtils.returnFail(ReturnEnum.GoodEnum.NONE);
    }

    /**
     * 批量添加折扣（excel）
     *
     * @param webFile    文件
     * @param sheetNames 表名
     * @return 影响条数
     */
    @ApiOperation("批量添加折扣（excel）")
    @PowerRequired(departmentId = "3002")
    @ApiImplicitParam(name = "sheetNames", value = "需要读取的表名", required = true, paramType = "query", allowMultiple = true, dataType = "String")
    @PostMapping(value = "/discountByExcel")
    public ReturnResult discountByExcel(@ApiParam(value = "需要读取的文件", required = true) MultipartFile webFile, String[] sheetNames) {
        File file = UploadUtils.upload(webFile);
        List<String> errorNames = Lists.newArrayList();
        for (String sheetName : sheetNames) {
            List<DiscountExcelParam> discountExcelParams = (List<DiscountExcelParam>) EasyExcelUtils.readExcelByModelSheetName(file,
                    sheetName, DiscountExcelParam.class);
            if (CollectionUtils.isEmpty(discountExcelParams)) {
                errorNames.add(sheetName);
                continue;
            }
            productWebService.discountByExcel(discountExcelParams);
        }
        if (CollectionUtils.isEmpty(errorNames)) {
            return ReturnResultUtils.returnSuccess();
        } else {
            return ReturnResultUtils.returnFail(ReturnEnum.ExcelEnum.SHEET_NAME, errorNames);
        }
    }

    /**
     * 删除商品
     *
     * @param productInfoParam 商品参数
     * @return 影响条数
     */
    @ApiOperation("删除商品")
    @PostMapping(value = "deleteProduct")
    @PowerRequired(departmentId = "3002")
    public ReturnResult deleteProduct(@Valid ProductInfoParam productInfoParam) {
        List<ProductInfoVo> productInfoVos = productWebService.show(productInfoParam);
        if (CollectionUtils.isEmpty(productInfoVos)) {
            return ReturnResultUtils.returnFail(ReturnEnum.GoodEnum.NONE);
        }
        productInfoVos.forEach(param -> {
            productWebService.deleteProduct(param.getProductId());
        });
        return ReturnResultUtils.returnSuccess();
    }

    /**
     * 商品打折查询
     *
     * @return 商品信息
     */
    @ApiOperation(value = "商品打折查询")
    @PostMapping(value = "/showProductDiscounts")
    public ReturnResult showProductDiscounts() {
        List<ProductDiscountsParam> productDiscountsParamList = productWebService.queryProductDiscount();
        return ReturnResultUtils.returnSuccess(productDiscountsParamList);
    }

    /**
     * 商品数量查询
     *
     * @return 商品数量
     */
    @ApiOperation(value = "商品数量查询")
    @PowerRequired(departmentId = "3002")
    @PostMapping(value = "/queryCount")
    public ReturnResult queryCount() {
        List<Map<String, Map<String, Integer>>> list = productWebService.queryCount();
        return ReturnResultUtils.returnSuccess(list);
    }

    /**
     * 查询商品
     *
     * @return
     */
    @ApiOperation(value = "商品查询")
    @PostMapping(value = "/merchandise")
    public ReturnResult merchandise() {
        List<Map<String, Map<String, List<ProductInfo>>>> list = productWebService.merchandise();
        return ReturnResultUtils.returnSuccess(list);
    }
}
