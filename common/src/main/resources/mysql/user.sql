CREATE DATABASE IF NOT EXISTS `e_user`;

USE `e_user`;

#用户表
DROP TABLE
IF EXISTS `user_info`;

CREATE TABLE `user_info` (
	`user_id` VARCHAR (11) NOT NULL COMMENT '用户id',
	`nickname` VARCHAR (20) NOT NULL COMMENT '用户昵称',
	`user_phone` VARCHAR (11) NOT NULL COMMENT '用户电话',
	`category_id` VARCHAR (11) NOT NULL COMMENT '用户类别',
	`user_openid` VARCHAR (64) COMMENT '用户微信openid',
	`user_email` VARCHAR(30) COMMENT '邮箱',
	`user_password` VARCHAR (32) NOT NULL COMMENT '密码',
	`user_age` INT (3) NOT NULL COMMENT '年龄',
	`user_sex` INT (1) NOT NULL COMMENT '性别 1为男，2为女，0未知，默认0',
	`delete_flag` INT (1) NOT NULL COMMENT '逻辑删除位 0有效，1删除',
	PRIMARY KEY (`user_id`)
) COMMENT = '用户信息表' DEFAULT CHARSET = utf8 AUTO_INCREMENT = 1 ENGINE = INNODB;

# 用户类别
DROP TABLE
IF EXISTS `user_category`;

CREATE TABLE `user_category` (
	`category_id` VARCHAR (32) NOT NULL COMMENT '类别编号',
	`category_name` VARCHAR (11) NOT NULL COMMENT '类别名称',
	PRIMARY KEY (`category_id`)
) COMMENT = '用户类别表' AUTO_INCREMENT = 1 DEFAULT CHARSET = utf8 ENGINE = INNODB;

#订单表
DROP TABLE
IF EXISTS `order_detail`;

CREATE TABLE `order_detail` (
	`order_id` VARCHAR (32) NOT NULL COMMENT '订单id',
	`user_id` VARCHAR (32) NOT NULL COMMENT '用户id',
	`product_id` VARCHAR (32) NOT NULL COMMENT '产品id',
	`product_num` INT (11) NOT NULL COMMENT '数量',
	`order_state` INT (11) NOT NULL COMMENT '订单状态',
	`create_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
	`update_time` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
	PRIMARY KEY (`order_id`)
) COMMENT = '订单详情表' DEFAULT CHARSET = utf8 ENGINE = INNODB;

# 订单状态表
DROP TABLE
IF EXISTS `order_state`;

CREATE TABLE `order_state` (
	`state_id` VARCHAR (11) NOT NULL COMMENT '类别编号',
	`state_name` VARCHAR (11) NOT NULL COMMENT '类别名称',
	PRIMARY KEY (`state_id`)
) COMMENT = '订单类别表' AUTO_INCREMENT = 1 DEFAULT CHARSET = utf8 ENGINE = INNODB;

# 购物车表
DROP TABLE
IF EXISTS `shopping_cart`;

CREATE TABLE `shopping_cart` (
	`user_id` VARCHAR (32) NOT NULL COMMENT '用户id',
	`product_id` VARCHAR (32) NOT NULL COMMENT '商品id',
	`product_num` INT (11) NOT NULL COMMENT '商品数量'
) COMMENT = '购物车' AUTO_INCREMENT = 1 DEFAULT CHARSET = utf8 ENGINE = INNODB;

# 会员表
DROP TABLE
IF EXISTS `vip_record`;

CREATE TABLE `vip_record` (
	`id` INT (11) AUTO_INCREMENT NOT NULL,
	`user_id` VARCHAR (64) NOT NULL COMMENT '用户id',
	`start_time` TIMESTAMP NOT NULL COMMENT '开始时间 精确到秒',
	`end_time` TIMESTAMP  DEFAULT CURRENT_TIMESTAMP COMMENT '结束时间 精确到秒',
	PRIMARY KEY (`id`)
) COMMENT = '会员记录' AUTO_INCREMENT = 1 DEFAULT CHARSET = utf8 ENGINE = INNODB;

