CREATE DATABASE IF NOT EXISTS `ebusiness`;

USE `ebusiness`;

DROP TABLE
    IF EXISTS `product_info`;

# 商品表

CREATE TABLE `product_info`
(
    `product_id`    VARCHAR(32)   NOT NULL,
    `product_name`  VARCHAR(32)   NOT NULL COMMENT '商品名称',
    `product_price` DECIMAL(8, 2) NOT NULL COMMENT '单价',
    `product_stock` INT(11)       NOT NULL COMMENT '库存',
    `category_id`   VARCHAR(30)   NOT NULL COMMENT '类别',
    `create_time`   TIMESTAMP     NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time`   TIMESTAMP              DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
    PRIMARY KEY (`product_id`)
) COMMENT = '商品表' DEFAULT CHARSET = utf8
                  ENGINE = INNODB;

DROP TABLE
    IF EXISTS `product_category`;

CREATE TABLE `product_category`
(
    `id`            INT(11) AUTO_INCREMENT NOT NULL,
    `category_id`   INT(11)                NOT NULL COMMENT '类别编号',
    `category_name` VARCHAR(32)            NOT NULL COMMENT '类别名称',
    PRIMARY KEY (`id`)
) COMMENT = '商品类别表' DEFAULT CHARSET = utf8
                    AUTO_INCREMENT = 1
                    ENGINE = INNODB;

DROP TABLE
    IF EXISTS `product_discount`;

CREATE TABLE `product_discount`
(
    `product_id`     VARCHAR(32) NOT NULL,
    `discount_level` INT(3)      NOT NULL COMMENT '折扣 0-100 100为未折扣 0为免费',
    `start_time`     TIMESTAMP   NOT NULL COMMENT '开始时间 精确到秒',
    `end_time`       TIMESTAMP   DEFAULT CURRENT_TIMESTAMP COMMENT '结束时间 精确到秒'
) COMMENT = '商品折扣表' DEFAULT CHARSET = utf8
                    ENGINE = INNODB;

#用户表

DROP TABLE
    IF EXISTS `user_info`;

CREATE TABLE `user_info`
(
    `user_id`       INT(11) AUTO_INCREMENT NOT NULL,
    `user_name`     VARCHAR(10)            NOT NULL COMMENT '用户名称',
    `user_phone`    VARCHAR(11)            NOT NULL COMMENT '用户电话',
    `category_id`   VARCHAR(32)            NOT NULL COMMENT '用户类别',
    `user_openid`   VARCHAR(64) COMMENT '用户微信openid',
    `user_password` VARCHAR(32)            NOT NULL COMMENT '密码',
    `user_age`      INT(3)                 NOT NULL COMMENT '年龄',
    `user_sex`      INT(1)                 NOT NULL COMMENT '性别 1为男，2为女，0未知，默认0',
    `is_delete`     INT(1)                 NOT NULL COMMENT '逻辑删除位 0有效，1删除',
    PRIMARY KEY (`user_id`)
) COMMENT = '用户信息表' DEFAULT CHARSET = utf8
                    AUTO_INCREMENT = 1
                    ENGINE = INNODB;

DROP TABLE
    IF EXISTS `user_category`;

CREATE TABLE `user_category`
(
    `id`            INT(11) AUTO_INCREMENT NOT NULL,
    `category_id`   VARCHAR(64)            NOT NULL COMMENT '类别',
    `category_name` INT(11)                NOT NULL COMMENT '类别编号',
    PRIMARY KEY (`id`)
) COMMENT = '用户类别表' AUTO_INCREMENT = 1
                    DEFAULT CHARSET = utf8
                    ENGINE = INNODB;

#订单表

DROP TABLE
    IF EXISTS `order_detail`;

CREATE TABLE `order_detail`
(
    `order_id`    VARCHAR(32) NOT NULL,
    `user_id`     VARCHAR(32) NOT NULL,
    `product_id`  VARCHAR(32) NOT NULL,
    `create_time` TIMESTAMP   NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time` TIMESTAMP            DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
    PRIMARY KEY (`order_id`)
) COMMENT = '订单详情表' DEFAULT CHARSET = utf8
                    ENGINE = INNODB;


#员工

DROP TABLE
    IF EXISTS `employee_info`;

CREATE TABLE `employee_info`
(
    `employee_id`     VARCHAR(11) NOT NULL COMMENT '员工编号',
    `employee_name`   VARCHAR(10) NOT NULL COMMENT '员工姓名',
    `employee_gender` CHAR(4)     NOT NULL COMMENT '性别',
    `role_id`         INT(11)     NOT NULL COMMENT '职位ID',
    `department_id`   varchar(32) COMMENT '部门id',
    `employee_email`  VARCHAR(20) NOT NULL COMMENT '邮箱',
    `employee_phone`  VARCHAR(11) NOT NULL COMMENT '手机号',
    `is_dimission`    int(1)      NOT NULL COMMENT '是否离职 默认0 未离职 1离职',
    `entry_time`      DATE        NOT NULL COMMENT '入职时间 精确到日',
    `dimission_time`  DATE COMMENT '离职时间 精确到日',
    PRIMARY KEY (`employee_id`)
) COMMENT = '员工信息表' ENGINE = INNODB
                    AUTO_INCREMENT = 1
                    DEFAULT CHARSET = utf8;



DROP TABLE
    IF EXISTS `employee_account`;

CREATE TABLE `employee_account`
(
    `employee_id` VARCHAR(11) NOT NULL COMMENT '员工编号',
    `account`     VARCHAR(11) NOT NULL COMMENT '账号',
    `password`    varchar(64) NOT NULL COMMENT '密码',
    `level`       VARCHAR(11) NOT NULL COMMENT '权限等级',
    PRIMARY KEY (`employee_id`)
) COMMENT = '员工账号表' ENGINE = InnoDB
                    AUTO_INCREMENT = 1
                    DEFAULT CHARSET = utf8;

-- 权限等级默认为职位等级 可更改但不能高于修改者的权限

DROP TABLE
    IF EXISTS `employee_role`;
CREATE TABLE `employee_role`
(
    `id`         int(11)     NOT NULL AUTO_INCREMENT,
    `role_id`    VARCHAR(11) NOT NULL COMMENT '职位id',
    `role_name`  varchar(64) NOT NULL COMMENT '职位名称',
    `role_level` VARCHAR(11) NOT NULL COMMENT '权限等级',
    PRIMARY KEY (`id`)
) COMMENT = '员工职位表' ENGINE = InnoDB
                    AUTO_INCREMENT = 1
                    DEFAULT CHARSET = utf8;

DROP TABLE
    IF EXISTS `employee_department`;
DROP TABLE IF EXISTS `employee_department`;
CREATE TABLE `employee_department`
(
    `id`              int(11)     NOT NULL AUTO_INCREMENT,
    `department_id`   varchar(32) NOT NULL COMMENT '部门id',
    `department_name` VARCHAR(20) NOT NULL COMMENT '部门名称',
    PRIMARY KEY (`id`)
) COMMENT = '员工部门表' ENGINE = InnoDB
                    AUTO_INCREMENT = 1
                    DEFAULT CHARSET = utf8;


DROP TABLE
    IF EXISTS `employee_attendance`;

CREATE TABLE `employee_attendance`
(
    `attendance_id`   BIGINT      NOT NULL AUTO_INCREMENT COMMENT 'id',
    `employee_id`     VARCHAR(64) NOT NULL COMMENT '员工编号',
    `attendance_time` TIMESTAMP DEFAULT CURRENT_TIMESTAMP COMMENT '打卡时间',
    PRIMARY KEY (`attendance_id`)
) COMMENT = '员工出勤表' ENGINE = INNODB
                    AUTO_INCREMENT = 1
                    DEFAULT CHARSET = utf8;