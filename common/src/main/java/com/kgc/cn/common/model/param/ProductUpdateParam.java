package com.kgc.cn.common.model.param;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created by boot on 2019/12/14
 */
@ApiModel("商品修改")
@Data
public class ProductUpdateParam implements Serializable {
    private static final long serialVersionUID = -994869432350396739L;
    @ApiModelProperty("商品id")
    private String productId;
    @ApiModelProperty("商品名称")
    private String productName;
    @ApiModelProperty(value = "商品价格", example = "0.00")
    private BigDecimal productPrice;
    @ApiModelProperty(value = "商品件数", example = "123")
    private Integer productStock;
    @ApiModelProperty("商品类别")
    private String categoryId;
}
