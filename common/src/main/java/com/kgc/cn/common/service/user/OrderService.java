package com.kgc.cn.common.service.user;

import com.kgc.cn.common.model.dto.OrderDetail;
import com.kgc.cn.common.model.vo.OrderDetailVo;

/**
 * Created by scy on 2019/12/23
 */


public interface OrderService {

    /**
     * 添加订单
     *
     * @param orderDetail 订单详情
     * @return 影响数
     */
    int addOrder(OrderDetail orderDetail);

    /**
     * 通过订单id查询订单
     *
     * @param orderId 订单id
     * @return 订单信息
     */
    OrderDetailVo selectOrder(String orderId, String userId);

    int addVipOrder(String userId);

    int changeState(String orderId, Integer state);
}
