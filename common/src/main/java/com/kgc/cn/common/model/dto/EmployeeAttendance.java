package com.kgc.cn.common.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class EmployeeAttendance implements Serializable {
    private static final long serialVersionUID = -2420963440014511646L;
    private Long attendanceId;

    private String employeeId;

    private Date attendanceTime;

}