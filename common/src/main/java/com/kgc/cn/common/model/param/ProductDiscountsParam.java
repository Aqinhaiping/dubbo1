package com.kgc.cn.common.model.param;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by xulei on 2019/12/18
 */
@Data
@ApiModel(value = "打折商品查询")
public class ProductDiscountsParam implements Serializable {


    private static final long serialVersionUID = -359978760562886664L;

    @ApiModelProperty(value = "商品id")
    private String productId;
    @ApiModelProperty(value = "商品名称")
    private String productName;
    @ApiModelProperty(value = "商品种类")
    private String categoryId;
    @ApiModelProperty(value = "商品原价", example = "1.00")
    private BigDecimal originalPrice;
    @ApiModelProperty(value = "折扣", example = "1")
    private Integer discountLevel;
    @ApiModelProperty(value = "折扣价", example = "1.00")
    private BigDecimal discountPrice;
    @ApiModelProperty(value = "开始时间")
    private Date startTime;
    @ApiModelProperty(value = "结束时间")
    private Date endTime;

}
