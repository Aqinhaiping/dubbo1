package com.kgc.cn.common.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class EmployeeDepartment implements Serializable {
    private static final long serialVersionUID = 4062528647111713786L;
    private Integer id;

    private String departmentId;

    private String departmentName;

}