package com.kgc.cn.common.utils.time;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 生日工具类
 */
public class BirthUtils {

    public static SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");

    public static boolean checkBirth(String bithday) {
        if (null == bithday) return false;
        String reg = "(([0-9]{3}[1-9]|[0-9]{2}[1-9][0-9]{1}|[0-9]{1}[1-9][0-9]{2}|[1-9][0-9]{3})-(((0[13578]|1[02])-(0[1-9]|[12][0-9]|3[01]))|((0[469]|11)-(0[1-9]|[12][0-9]|30))|(02-(0[1-9]|[1][0-9]|2[0-8]))))|((([0-9]{2})(0[48]|[2468][048]|[13579][26])|((0[48]|[2468][048]|[3579][26])00))-02-29) ";
        return bithday.matches(reg);

    }

    public static Date toDate(String birthday) {
        Date date = null;
        try {
            date = sf.parse(birthday);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public static String toString(Date birthday) {
        String date = null;
        date = sf.format(birthday);
        return date;
    }

}
