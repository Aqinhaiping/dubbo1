package com.kgc.cn.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Created by scy on 2019/12/23
 */

@Getter
@AllArgsConstructor
public enum MailTemplates {
    ACCOUNT("account.ftl"),
    PASSWORD("resetPassword.ftl"),
    VERIFY("verify.ftl"),
    VIP("vip.ftl");

    private String templateName;
}
