package com.kgc.cn.common.utils.exception.exception;

import com.kgc.cn.common.utils.exception.enums.ExceptionEnum;
import lombok.Data;

/**
 * Created by scy on 2019/12/13
 */

@Data
public class EmployeeException extends RuntimeException {
    private int code;
    private String message;
    private Object data;

    public EmployeeException() {
        ExceptionEnum exceptionEnum = ExceptionEnum.getByEClass(this.getClass());
        if (exceptionEnum != null) {
            code = exceptionEnum.getResultCode().getCode();
            message = exceptionEnum.getResultCode().getMessage();
        }

    }

    public EmployeeException(String message) {
        this();
        this.message = message;
    }
}
