package com.kgc.cn.common.model.param;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by scy on 2019/12/16
 */

@Data
public class EmployeeCheckOut implements Serializable {
    private static final long serialVersionUID = 8590274971892176091L;

    private String employeeId;
    private String employeeName;
    private String employeeGender;
    private String role;
    private String department;
    private String employeeEmail;
    private String employeePhone;
    private Date entryTime;
    private String isDimission;
    private Date dimissionTime;

}
