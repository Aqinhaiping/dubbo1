package com.kgc.cn.common.utils.exception.enums;


public enum ResultCode {
    PARAM_IS_INVALID(10001, "参数不合法"),
    RESULE_DATA_NONE(10002, "数据不存在"),
    USER_NOTLOGIN(20001, "用户未登录"),
    LIMITEDPOWER(30000, "权限不足");


    private int code;
    private String message;

    ResultCode(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
