package com.kgc.cn.common.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class VipRecord implements Serializable {
    private static final long serialVersionUID = 785996855556154499L;
    private Integer id;

    private String userId;

    private Date startTime;

    private Date endTime;


}