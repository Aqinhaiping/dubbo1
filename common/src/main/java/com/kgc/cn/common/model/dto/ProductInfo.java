package com.kgc.cn.common.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProductInfo implements Serializable {
    private static final long serialVersionUID = 5384449458388021464L;
    private String productId;

    private String productName;

    private BigDecimal productPrice;

    private Integer productStock;

    private String categoryId;

    private Date createTime;

    private Date updateTime;


}