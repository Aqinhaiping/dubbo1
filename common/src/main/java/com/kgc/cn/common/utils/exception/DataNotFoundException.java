package com.kgc.cn.common.utils.exception;


import com.kgc.cn.common.utils.exception.exception.BusinessException;

public class DataNotFoundException extends BusinessException {

    public DataNotFoundException() {
        super();
    }

    public DataNotFoundException(Object data) {
        super();
        super.setData(data);
    }
}
