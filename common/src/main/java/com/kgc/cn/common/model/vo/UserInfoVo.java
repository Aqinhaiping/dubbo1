package com.kgc.cn.common.model.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserInfoVo implements Serializable {
    private static final long serialVersionUID = 8378785430970347201L;
    private String userId;

    private String nickname;

    private String userPhone;

    private String categoryId;

    private String userOpenid;

    private String userEmail;

    private String userPassword;

    private Integer userAge;

    private Integer userSex;

    private Integer deleteFlag;

    public UserInfoVo createId() {
        userId = UUID.randomUUID().toString().substring(0, 8);
        return this;
    }

}