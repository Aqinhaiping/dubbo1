package com.kgc.cn.common.model.vo;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class EmployeeInfoVo implements Serializable {
    private static final long serialVersionUID = 800739182611851808L;
    private String employeeId;

    private String employeeName;

    private String employeeGender;

    private Integer roleId;

    private String departmentId;

    private String employeeEmail;

    private String employeePhone;

    private Integer isDimission;

    @JSONField(format = "yyyy-MM-dd")
    private Date entryTime;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date dimissionTime;

    public EmployeeInfoVo createId() {
        employeeId = departmentId + UUID.randomUUID().toString().replace("-", "").substring(0, 4);
        return this;
    }
}
