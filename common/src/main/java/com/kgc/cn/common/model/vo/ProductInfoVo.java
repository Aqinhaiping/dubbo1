package com.kgc.cn.common.model.vo;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.UUID;


/**
 * 商品实体VO
 */
@Data
@ApiModel("商品类")
public class ProductInfoVo implements Serializable {

    private static final long serialVersionUID = -1785961742348053780L;
    @ApiModelProperty("商品id")
    private String productId;
    @ApiModelProperty("商品名")
    private String productName;
    @ApiModelProperty(value = "价格", example = "0.00")
    private BigDecimal productPrice;
    @ApiModelProperty(value = "库存", example = "100")
    private Integer productStock;
    @ApiModelProperty("类别id")
    private String categoryId;
    @ApiModelProperty("入库时间")
    private Date createTime;
    @ApiModelProperty("修改时间")
    private Date updateTime;

    public ProductInfoVo createId() {
        productId = categoryId + UUID.randomUUID().toString().replace("-", "").substring(0, 4);
        return this;
    }

}