package com.kgc.cn.common.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class EmployeeInfo implements Serializable {
    private static final long serialVersionUID = 4865931403069238577L;
    private String employeeId;

    private String employeeName;

    private String employeeGender;

    private Integer roleId;

    private String departmentId;

    private String employeeEmail;

    private String employeePhone;

    private Integer isDimission;

    private Date entryTime;

    private Date dimissionTime;

    public EmployeeInfo createId() {
        employeeId = departmentId + UUID.randomUUID().toString().substring(0, 4);
        return this;
    }
}