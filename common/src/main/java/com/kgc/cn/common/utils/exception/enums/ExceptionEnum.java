package com.kgc.cn.common.utils.exception.enums;

import com.kgc.cn.common.utils.exception.DataNotFoundException;
import com.kgc.cn.common.utils.exception.LimitedPowerException;
import com.kgc.cn.common.utils.exception.ParameterInvalidException;
import com.kgc.cn.common.utils.exception.UserNotLoginException;
import lombok.Getter;

@Getter
public enum ExceptionEnum {

    /**
     * 无效参数
     */
    PARAMETER_INVALID(ParameterInvalidException.class, ResultCode.PARAM_IS_INVALID),

    /**
     * 数据未找到
     */
    NOT_FOUND(DataNotFoundException.class, ResultCode.RESULE_DATA_NONE),

    /**
     * 用户未登录
     */
    USER_NOTLOGIN(UserNotLoginException.class, ResultCode.USER_NOTLOGIN),


    /**
     * 权限不足
     */
    FORBIDDEN(LimitedPowerException.class, ResultCode.LIMITEDPOWER);
    /**
     * 数据已存在
     */
    /*CONFLICT(DataConflictException.class, HttpStatus.CONFLICT, ResultCode.DATA_ALREADY_EXISTED),*/

    /**
     * 无访问权限
     */
    /*FORBIDDEN(PermissionForbiddenException.class, HttpStatus.FORBIDDEN, ResultCode.PERMISSION_NO_ACCESS),*/
    /**
     * 远程访问时错误
     */
    /*REMOTE_ACCESS_ERROR(RemoteAccessException.class, HttpStatus.INTERNAL_SERVER_ERROR, ResultCode.INTERFACE_OUTTER_INVOKE_ERROR),*/
    /**
     * 系统内部错误
     *//*
    INTERNAL_SERVER_ERROR(InternalServerException.class, HttpStatus.INTERNAL_SERVER_ERROR, ResultCode.SYSTEM_INNER_ERROR);*/


    private Class<? extends RuntimeException> eClass;

    private ResultCode resultCode;

    ExceptionEnum(Class<? extends RuntimeException> eClass, ResultCode resultCode) {
        this.eClass = eClass;
        this.resultCode = resultCode;
    }

    public static ExceptionEnum getByEClass(Class<? extends RuntimeException> eClass) {
        if (eClass == null) {
            return null;
        }

        for (ExceptionEnum exceptionEnum : ExceptionEnum.values()) {
            if (eClass.equals(exceptionEnum.eClass)) {
                return exceptionEnum;
            }
        }

        return null;
    }

}
