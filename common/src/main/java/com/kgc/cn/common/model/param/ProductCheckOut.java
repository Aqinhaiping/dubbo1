package com.kgc.cn.common.model.param;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by scy on 2019/12/16
 */

@Data
public class ProductCheckOut implements Serializable {

    private static final long serialVersionUID = -1723156452747605469L;
    private String productId;
    private String productName;
    private BigDecimal productPrice;
    private Integer productStock;
    private String category;
    private Date createTime;
    private Date updateTime;
}
