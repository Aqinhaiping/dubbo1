package com.kgc.cn.common.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ShoppingCart implements Serializable {
    private static final long serialVersionUID = 7813482618378715132L;
    private String userId;

    private String productId;

    private Integer productNum;

}