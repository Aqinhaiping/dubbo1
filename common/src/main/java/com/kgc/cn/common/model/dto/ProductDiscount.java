package com.kgc.cn.common.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProductDiscount implements Serializable {
    private static final long serialVersionUID = 2272872009220221898L;
    private String productId;

    private Integer discountLevel;

    private Date startTime;

    private Date endTime;


}