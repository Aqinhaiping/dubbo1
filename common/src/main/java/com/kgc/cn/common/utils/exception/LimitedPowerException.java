package com.kgc.cn.common.utils.exception;

import com.kgc.cn.common.utils.exception.exception.EmployeeException;

/**
 * Created by scy on 2019/12/13
 */


public class LimitedPowerException extends EmployeeException {
    public LimitedPowerException() {
        super();
    }

    public LimitedPowerException(Object data) {
        super();
        super.setData(data);
    }
}
