package com.kgc.cn.common.utils.randoms;

import java.util.Random;

/**
 * Created by boot on 2019/12/25
 */
public class RandomsUtils {

    /**
     * 获取随机数字字符串
     *
     * @param num 长度
     * @return 字符串
     */
    public static String getRandom(int num) {
        StringBuilder sbd = new StringBuilder();
        for (int i = 0; i < num; i++) {
            sbd.append(new Random().nextInt(10));
        }
        return sbd.toString();
    }
}
