package com.kgc.cn.common.service.third;

import com.kgc.cn.common.model.dto.OrderDetail;

import java.util.Map;

/**
 * Created by scy on 2019/12/21
 */


public interface WxService {

    String getUserInfo(String code);

    Map<String, String> pay(OrderDetail orderDetail) throws Exception;

    boolean isSignatureValid(String xml) throws Exception;

    String toLogin(String state) throws Exception;

    Map<String, String> xmlToMap(String xml) throws Exception;
}
