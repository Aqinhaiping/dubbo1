package com.kgc.cn.common.model.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ShoppingCartVo implements Serializable {
    private static final long serialVersionUID = 8154009257009192266L;
    private String userId;

    private String productId;

    private Integer productNum;

}