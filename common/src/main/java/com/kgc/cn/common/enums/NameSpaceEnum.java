package com.kgc.cn.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Created by scy on 2019/12/24
 */

@Getter
@AllArgsConstructor
public enum NameSpaceEnum {
    VIP("userVip:"),
    PHONE_CODE("phoneCode:"),
    EMAIL_CODE("emailCode:"),
    PRODUCT_AMOUNT("productAmount:");

    String namespace;
}
