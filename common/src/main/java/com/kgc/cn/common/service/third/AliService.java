package com.kgc.cn.common.service.third;

import com.kgc.cn.common.model.param.SmsParam;

/**
 * Created by scy on 2019/12/21
 */


public interface AliService {
    void sendSms(SmsParam smsParam);
}
