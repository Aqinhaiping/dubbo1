package com.kgc.cn.common.utils.stringid;

/**
 * Created by scy on 2019/12/23
 */


public class StringIntUtils {
    public static int stringToInt(String str) {
        char[] chars = str.toLowerCase().toCharArray();
        StringBuffer sbf = new StringBuffer();
        for (char c : chars) {
            sbf.append(charToIntString(c));
        }
        return sbf.length() > 10 ? Integer.parseInt(sbf.toString().substring(0, 10)) : Integer.parseInt(sbf.toString());
    }

    /**
     * a-z to 1-26
     *
     * @param c char
     * @return int
     */
    public static String charToIntString(char c) {
        switch (c) {
            case 'a':
            case '1':
                return "1";
            case 'b':
            case '2':
                return "2";
            case 'c':
            case '3':
                return "3";
            case 'd':
            case '4':
                return "4";
            case '5':
                return "5";
            case 'f':
            case '6':
                return "6";
            case 'g':
            case '7':
                return "7";
            case 'h':
            case '8':
                return "8";
            case 'i':
            case '9':
                return "9";
            case 'j':
                return "10";
            case 'k':
                return "11";
            case 'l':
                return "12";
            case 'm':
                return "13";
            case 'n':
                return "14";
            case 'o':
                return "15";
            case 'p':
                return "16";
            case 'q':
                return "17";
            case 'r':
                return "18";
            case 's':
                return "19";
            case 't':
                return "20";
            case 'u':
                return "21";
            case 'v':
                return "22";
            case 'w':
                return "23";
            case 'x':
                return "24";
            case 'y':
                return "25";
            case 'z':
                return "26";
            default:
                return "0";

        }
    }
}
