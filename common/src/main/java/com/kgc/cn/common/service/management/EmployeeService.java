package com.kgc.cn.common.service.management;

import com.github.pagehelper.PageInfo;
import com.kgc.cn.common.model.dto.EmployeeAccount;
import com.kgc.cn.common.model.param.AttendanceParam;
import com.kgc.cn.common.model.param.EmployeeCheckOut;
import com.kgc.cn.common.model.param.EmployeeParam;
import com.kgc.cn.common.model.vo.EmployeeInfoVo;

import java.util.List;

/**
 * Created by scy on 2019/12/10
 */


public interface EmployeeService {
    /**
     * 添加员工信息
     *
     * @param employeeInfoVo 员工信息
     * @return 影响数
     */
    int addEmployee(EmployeeInfoVo employeeInfoVo);


    /**
     * 员工登录
     *
     * @param phone    手机号
     * @param password 密码
     * @return 员工信息
     */
    EmployeeInfoVo toLogin(String phone, String password);


    /**
     * 添加员工账号
     *
     * @param employeeAccount 员工账号
     * @return 影响数
     */
    int addEmployeeAccount(EmployeeAccount employeeAccount);


    /**
     * 删除员工
     *
     * @param employeeId 员工编号
     * @return 影响数
     */
    int delEmployee(String employeeId);


    /**
     * 员工查询（模糊）
     *
     * @param employeeParam 员工信息
     * @return list集合
     */
    List<EmployeeInfoVo> queryEmployee(EmployeeParam employeeParam);

    /**
     * 打卡
     *
     * @param employeeId 员工编号
     * @return 影响条数
     */
    int addEmployeeAttendance(String employeeId);


    /**
     * 通过员工编号查询离职状态
     *
     * @param employeeId 员工编号
     * @return 员工信息
     */
    EmployeeInfoVo queryEmployeeInfo(String employeeId);

    /**
     * 员工信息修改
     *
     * @param employeeInfoVo 员工信息
     * @return 员工信息
     */
    int updateEmployeeInfo(EmployeeInfoVo employeeInfoVo);

    /**
     * 员工信息检出
     *
     * @return 员工信息集合
     */
    List<EmployeeCheckOut> checkOut();

    /**
     * 账号修改
     *
     * @param employeeAccount 账号信息
     * @return 影响条数
     */
    int updateEmployeeAccount(EmployeeAccount employeeAccount);

    /**
     * 员工分页
     *
     * @param employeeParam 员工信息
     * @param page          页数
     * @param size          条数
     * @return 员工信息
     */
    PageInfo<EmployeeInfoVo> paging(EmployeeParam employeeParam, int page, int size);

    /**
     * 查询出勤
     *
     * @param flag true 已出勤 false 未出勤
     * @return 出勤或未出勤信息
     */
    PageInfo<AttendanceParam> queryAttendance(boolean flag, int page, int size);


}
