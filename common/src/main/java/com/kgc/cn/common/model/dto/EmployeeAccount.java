package com.kgc.cn.common.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class EmployeeAccount implements Serializable {
    private static final long serialVersionUID = 2965488359639420775L;
    private String employeeId;

    private String account;

    private String password;

    private String level;


}