package com.kgc.cn.common.service.third;

import com.kgc.cn.common.enums.MailTemplates;
import com.kgc.cn.common.model.param.mail.MailParam;

/**
 * Created by scy on 2019/12/23
 */


public interface MailService {
    void sendTemplateMail(MailParam mailParam, MailTemplates mailTemplates);
}
