package com.kgc.cn.common.utils.copy;

import com.google.common.collect.Lists;
import org.springframework.beans.BeanUtils;

import java.util.List;

/**
 * Created by scy on 2019/12/9
 */
public class CopyUtils {
    /**
     * S(Source) to T(Target)
     *
     * @param s     原文件
     * @param clazz 目标文件类
     * @param <S>   源文件类型
     * @param <T>   目标文件类型
     * @return 目标文件
     */
    public static <S, T> T copy(S s, Class<T> clazz) {
        T t = null;
        try {
            t = clazz.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        BeanUtils.copyProperties(s, t);
        return t;
    }

    /**
     * List<S>(Source) to List<T>(Target)
     *
     * @param sList 原集合
     * @param clazz 目标集合类型
     * @param <S>   原集合泛型
     * @param <T>   目标集合泛型
     * @return 目标集合
     */
    public static <S, T> List<T> copyList(List<S> sList, Class<T> clazz) {
        List<T> tList = Lists.newArrayList();
        sList.forEach(param -> {
            tList.add(copy(param, clazz));
        });
        return tList;
    }

}
