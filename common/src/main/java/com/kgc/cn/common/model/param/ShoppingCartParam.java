package com.kgc.cn.common.model.param;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("购物车商品增加")
public class ShoppingCartParam implements Serializable {
    private static final long serialVersionUID = 8154009257009192266L;

    @ApiModelProperty("商品id")
    private String productId;

    @ApiModelProperty(value = "商品数量", example = "1")
    private Integer productNum;

}