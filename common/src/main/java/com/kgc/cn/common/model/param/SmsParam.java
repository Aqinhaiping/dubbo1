package com.kgc.cn.common.model.param;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Map;

/**
 * Created by scy on 2019/12/21
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SmsParam implements Serializable {
    private static final long serialVersionUID = 5571257426968338475L;
    private String toPhone;
    private String templateCode;
    Map<String, String> templateParams;
}
